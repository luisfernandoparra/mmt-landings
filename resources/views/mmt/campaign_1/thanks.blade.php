<!DOCTYPE html>
<html lang="es">
<head>
  <title>MMT Seguros</title>
  <meta charset="utf-8">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="copyright" content="MMT Seguros. Todos los derechos reservados.">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700,900" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="{{ asset('css/mmt/campaign_1/mmt.css') }}">
  <link rel="shortcut icon" href="{{ asset('imgs/mmt/campaign_1/mmt-favicon.png') }}" type="image/png"/>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

</head>
<body>
@if (!empty($id))
<img src="https://eretail.uinterbox.com/tracking/cnv?org=1122&evt=2189&cid={{ $id }}" height="1" width="1">
@endif
<div class="container top text-center">

      <div class="row">
        <div class="col-sm-4 logo_MMT"><img src="{{ asset('imgs/mmt/campaign_1/logo_MMT.jpg') }}" alt="MMT Seguros" height="88" width="172"></div>
        <div class="col-sm-8 text-top">&iquest;Tu seguro de hogar te devuelve dinero si no lo usas? </div>
      </div>
</div>


 <div class="jumbotron jumbotron-fluid image-cover">
<div class="container" style="margin-top:0px;">
  <div class="row">
    <div class="col-sm-7" style="min-height:500px;">
      <div class="detalles">
      <p>"Me Gusta": la &uacute;nica p&oacute;liza de seguro<br><span class="yell">que te devuelve dinero si no lo usas</span><br>
       Si no das partes, te devuelve una parte <span class="yell">directamente a tu cuenta</span> </p>
      </div>
    </div>
    <div class="col-sm-5" style="background-color:#fff;padding:22px;">
    <span class="destacado">Gracias por interesarte en nuestro seguro,en breve nos pondremos en contacto contigo para darte m&aacute;s informaci&oacute;n.</span>
    <div class="text-center">
    <img src="{{ asset('imgs/mmt/campaign_1/recibido.jpg') }}" alt="Recibido" class="recibido">
    <img src="{{ asset('imgs/mmt/campaign_1/logo_MMT.jpg') }}" alt="MMT Seguros" height="88" width="172"><br>
    <span class="claim">SI&Eacute;NTETE MMT</span><br>
    <p class="claim">Si&eacute;ntete mucho m&aacute;s tranquilo</p>
    </div>


    </div>
  </div>
</div>
</div>


<div class="jumbotron bottom jumbotron-fluid text-center">
  <p>Mutua MMT Seguros</p>
</div>


<!-- Modal MMT -->
<div class="modal" id="Modal_MMT">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h6 class="modal-title">Pol&iacute;tica de Protecci&oacute;n de Datos</h6>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">

      <strong>Responsable de los datos</strong><br>
        <p>Mutua MMT Seguros, Sociedad Mutua de Seguros a Prima Fija, inscrita en el Registro Mercantil de Madrid en el Libro de Mutuas hoja 16-1<sup>a</sup>, con NIF G28010817.</p>
        <strong>Domicilio del DPO</strong><br>
        <p>Domicilio social: C/ Trafalgar n<sup>o</sup> 11 - 28010 MADRID</p>
        <strong>Finalidades </strong><br>
        <p>Asesoramiento comercial y elaboraci&oacute;n de presupuestos sobre producos de MMT Seguros y Empresas Colaboradoras mediante llamadas, o env&iacute;o por cualquier medio digital de informaci&oacute;n y publicidad, sobre ofertas de productos y servicios que MMT Seguros comercialice.</p>
        <strong>Legitimaci&oacute;n</strong><br>
        <p>El consentimiento otorgado al solicitar informaci&oacute;n de nuestros productos</p>
        <strong>Destinatarios  </strong><br>
        <p>No se ceder&aacute;n los datos a terceros, salvo obligaci&oacute;n legal. </p>
        <strong>Plazos </strong><br>
        <p>Un a&ntilde;o desde el momento de la recogida de los datos.</p>
        <strong>Derechos</strong><br>
        <p>Podr&aacute;s ejercitar tus derechos de acceso, rectificaci&oacute;n, supresi&oacute;n, portabilidad de los datos, limitaci&oacute;n u oposici&oacute;n a su tratamiento.</p>
        <strong>D&oacute;nde ejercitar tus derechos</strong><br>
        <p>Enviando un e-mail a dpo@mmtsuros.es o remitiendo tu solicitud a la C/Trafalgar, 11 (28010 - Madrid). Ser&aacute; necesario aportar copia de tu DNI o documeno oficial que te acredite. Puedes presentar igualmente una reclamaci&oacute;n ante la Agencia Espa&ntilde;ola de Protecci&oacute;n de Datos, para lo que dispones de la informaci&oacute;n necesaria en: www.agpd.es</p>
        <strong class="underline"><a href="#Modal_adicional" data-toggle="modal">Informaci&oacute;n adicional</a></strong><br><br>

      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <!-- button type="button" class="btn btn-danger" data-dismiss="modal">Close</button-->
      </div>

    </div>
  </div>
</div>

<!-- Modal Adicional -->
<div class="modal" id="Modal_adicional">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h6 class="modal-title">Pol&iacute;tica de Protecci&oacute;n de Datos</h6>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <p>Esta web es propiedad de Mutua MMT Seguros, Sociedad Mutua de Seguros a Prima Fija, entidad inscrita en el Registro Mercantil de Madrid en el Libro de Mutuas hoja 16-1<sup>a</sup>. Con CIF G-28010817. Domicilio social: C/ Trafalgar n<sup>o</sup> 11 - 28010 MADRID </p>
        <p>Debido a las actualizaciones y modificaciones que se pueden realizar, el usuario, cada vez que acceda a la presente Web, debe leer atentamente la siguiente Pol&iacute;tica de privacidad. Regir&aacute;, en cualquier caso, la que est&eacute; publicada en la presente Web en el momento en que se acceda a la misma. Mutua MMT Seguros se reserva el derecho a actualizar, modificar o eliminar, en cualquier momento y sin previo aviso, la informaci&oacute;n contenida en esta Web lo que no conlleva ninguna responsabilidad por los da&uacute;os y perjuicios que se pudieran causar por esta acci&oacute;n.</p>
        <p>Este sitio web puede contener enlaces a otros sitios que resulten ser de su inter&eacute;s. Una vez que usted de clic en estos enlaces y abandone nuestra p&aacute;gina, ya no tendr&iacute;amos control sobre el sitio al que es redirigido y, por lo tanto, no ser&iacute;amos responsables de los t&eacute;rminos o privacidad, ni de la protecci&oacute;n de sus datos en esos otros sitios de terceros, que est&aacute;n sujetos a sus propias pol&iacute;ticas de privacidad por lo cual es recomendable que los consulte para confirmar que usted est&aacute; de acuerdo con &eacute;stas.</p>
        <p>Los datos que libre y voluntariamente nos facilite a trav&eacute;s de formularios, tarificadores para la obtenci&oacute;n de presupuestos de seguros, correo electr&oacute;nico o cualquier otro medio digital, para la informaci&oacute;n previa y para la formalizaci&oacute;n del contrato, as&iacute; como los proporcionados durante la vigencia del mismo, los que se obtengan mediante grabaci&oacute;n de conversaciones telef&oacute;nicas y los generados por la navegaci&oacute;n por este sitio Web, ser&aacute;n tratados en base a lo dispuesto en la normativa vigente relativa a la protecci&oacute;n de los datos de car&aacute;cter personal. </p>
        <p>La visita a este sitio Web no implica que el usuario est&eacute; obligado a facilitar ninguna informaci&oacute;n sobre si mismo. En el caso de que el usuario facilite alguna informaci&oacute;n de car&aacute;cter personal, los datos recogidos en este sitio Web ser&aacute;n tratados de forma leal, l&iacute;cita y transparente con el usuario, con sujeci&oacute;n en todo momento a los principios y derechos recogidos en el Reglamento (UE) 2016/679 del Parlamento Europeo y del Consejo, relativo a la protecci&oacute;n de datos de las personas f&iacute;sicas en lo que respecta al tratamiento de datos personales y a la libre circulaci&oacute;n de estos datos. </p>
        <p>Los datos de car&aacute;cter personal proporcionados por los usuarios deber&aacute;n ser exactos y puestos al d&iacute;a, de forma que respondan con veracidad a la situaci&oacute;n actual del interesado. Por ello, el usuario deber&aacute; responder de la certeza de los datos personales suministrados y comunicar cualquier modificaci&oacute;n de los mismos que se produzca en un futuro.</p>
        <p>Mutua MMT Seguros proceder&aacute; a la supresi&oacute;n de los datos personales recogidos cuando dejen de ser necesarios o pertinentes para la finalidad para la que hubieren sido recabados o registrados. Dicha supresi&oacute;n dar&aacute; lugar al bloqueo de los datos, conserv&aacute;ndose &uacute;nicamente a disposici&oacute;n de las Administraciones P&uacute;blicas, Jueces y Tribunales durante el plazo de prescripci&oacute;n. Cumplido el citado plazo se proceder&aacute; a la supresi&oacute;n de sus datos personales salvo que sean necesarios para el cumplimiento de una obligaci&oacute;n legal que requiera el tratamiento de datos impuesta por el Derecho de la Uni&oacute;n o de los Estados miembros que se aplique al responsable del tratamiento, o para el cumplimiento de una misi&oacute;n realizada en inter&eacute;s p&uacute;blico o en el ejercicio de poderes p&uacute;blicos conferidos al responsable o para la formulaci&oacute;n, el ejercicio o la defensa de reclamaciones.</p>
        <p>Mutua MMT Seguros garantiza que todos los datos personales facilitados por el usuario ser&aacute;n utilizados con la finalidad, forma, limitaciones y derechos previstos en la normativa en vigor. Para ejercitar gratuitamente sus derechos de acceso, rectificaci&oacute;n, supresi&oacute;n, limitaci&oacute;n en el tratamiento, portabilidad (cuando sea t&eacute;cnicamente posible), oposici&oacute;n y revocaci&oacute;n del consentimiento ante el Delegado de Protecci&oacute;n de Datos, deber&aacute; enviar un correo electr&oacute;nico a la siguiente direcci&oacute;n dpo@mmtsuros.es o remitir su solicitud por correo postal a la c/Trafalgar, 11 (28010) Madrid. Ser&aacute; necesario aportar copia de su DNI o documento oficial que le acredite. Puede presentar igualmente una reclamaci&oacute;n ante la Agencia Espa&uacute;ola de Protecci&oacute;n de Datos, para lo que dispone de la informaci&oacute;n necesaria en: www.agpd.es.</p>

      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <!-- button type="button" class="btn btn-danger" data-dismiss="modal">Close</button-->
      </div>

    </div>
  </div>
</div>


</body>
</html>