<!DOCTYPE html>
<html lang="es">
<head>
  <title>MMT Seguros</title>
  <meta charset="utf-8">
  <meta name="_token" content="{{ csrf_token() }}" />
  <meta name="description" content="'Me Gusta': la única póliza de seguro que te devuelve dinero si no lo usas Si no das partes, te devuelve una parte directamente a tu cuenta">
  <meta name="keywords" content="seguros,póliza,mmt">
  <meta name="copyright" content="MMT Seguros. Todos los derechos reservados.">
  <meta property="og:type" content="website" />
	<meta property="og:title" content="MMT-Seguros">
	<meta property="og:site_name" content="MMT-Seguros Hogar">
	<meta property="og:url" content="http://">
	<meta property="og:description" content="'Me Gusta': la única póliza de seguro que te devuelve dinero si no lo usas Si no das partes, te devuelve una parte directamente a tu cuenta">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700,900" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="{{ asset('css/mmt/campaign_1/mmt.css') }}">
  <link rel="stylesheet" href="{{ asset('css/cookiealert.css') }}"> 
 
  <link rel="shortcut icon" href="{{ asset('imgs/mmt/campaign_1/mmt-favicon.png') }}" type="image/png"/>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  
  
</head>

<body>

<div class="container top text-center">

      <div class="row">
        <div class="col-sm-4 logo_MMT"><img src="{{ asset('imgs/mmt/campaign_1/logo_MMT.jpg') }}" alt="MMT Seguros" height="88" width="172"></div>
        <div class="col-sm-8 text-top">&iquest;Tu seguro de hogar te devuelve dinero si no lo usas? </div>
      </div>
</div>


 <div class="jumbotron jumbotron-fluid image-cover">
<div class="container" style="margin-top:0px;">
  <div class="row">
    <div class="col-sm-7" style="min-height:500px;">
      <div class="detalles">
      <p>"Me Gusta": la &uacute;nica p&oacute;liza de seguro<br><span class="yell">que te devuelve dinero si no lo usas</span><br>
       Si no das partes, te devuelve una parte <span class="yell">directamente a tu cuenta</span> </p>

      </div>
    </div>
    <div class="col-sm-5" style="background-color:#fff;padding:22px;">
    <span class="destacado">&iexcl;Te informamos sin compromiso!<br>
    El seguro que te devuelve dinero </span>
    <div class="alert alert-danger alert-dismissible fade show" style="display: @if($errors->any()) block @else none @endif" role="alert">
      <div id="div_errors">
      @if($errors->any())
          @foreach ($errors->all() as $error)
              <div>{{ $error }}</div>
          @endforeach
      @endif
  </div>
      <button type="button"  class="close" data-dismiss="alert" aria-label="Close" onclick="jQuery('.alert').alert('close');">
              <span aria-hidden="true">&times;</span>
            </button>
  </div>
    <form  name='forms' id='forms' method='post' action="">
      {{  csrf_field() }}
      <input type="hidden" id="id_source" name="id_source" value="{{ $source }}" />
      <input type="hidden" id="id_site" name="id_site" value="1" />
      <input type="hidden" id="validEmail" name="validEmail" value="0" />
      <!-- End Hidden Vars -->
      <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
        <label for="name">Nombre:</label>
        <input type="text" class="form-control" id="name" name="name" value='{{old('name')}}' required>
      </div>

        <div class="form-group  {{ $errors->has('phone') ? ' has-error' : '' }}">
        <label for="phone">Tel&eacute;fono:</label>
        <input pattern=".{9}"  type='number' class="form-control" id="phone" name="phone" value='{{old('phone')}}' required>
        </div>

        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
        <label for="email">Email:</label>
        <input type='email' id='email' name='email' value='{{old('email')}}' class="form-control" required>
        </div>
        <label>Tipo de vivienda:</label><br>
         <div class="form-group form-inline {{ $errors->has('hometype') ? ' has-error' : '' }}">
@php
    

@endphp
          <div class="custom-control custom-radio custom-control-inline">
              <input type="radio" class="custom-control-input" id="hometype_own" name="hometype" value="propiedad" @if (old('hometype')=='propiedad') checked @endif required>
              <label class="custom-control-label" for="hometype_own" >Propia</label>
          </div>

          <div class="custom-control custom-radio custom-control-inline">
              <input type="radio" class="custom-control-input" id="hometype_rent" name="hometype" value="alquiler" @if (old('hometype')=='alquiler') checked @endif required>
              <label class="custom-control-label" for="hometype_rent">Alquiler</label>
          </div>
        </div>

        <div class="form-group {{ $errors->has('enddatepolicy') ? ' has-error' : '' }}">
        <label for="enddatepolicy">Fecha de vencimiento del seguro actual:</label>
        <input type="date" id="enddatepolicy" name="enddatepolicy" class="form-control" value="{{ old('enddatepolicy') }}" required>
        </div>
        <div class="form-group form-check {{ $errors->has('legalpolicy') ? ' has-error' : '' }}">
            <label class="form-check-label size12">
            <input class="form-check-input" type="checkbox" id="legalpolicy" name="legalpolicy" value="1" required> He le&iacute;do y acepto la <span class="underline"><a href="#Modal_MMT" data-toggle="modal">Pol&iacute;tica de Protecci&oacute;n de Datos</a></span>
            </label>
        </div>

        <button type="submit" id="btnSubmit" name="btnSubmit" class="btn btn-primary btn-especial">ENVIAR</button>
    </form>


    </div>
  </div>
</div>
</div>

<!-- START Bootstrap-Cookie-Alert -->
<div class="alert text-center cookiealert" role="alert">
    <b>¿Te gustan las Cookies?</b> &#x1F36A; Utilizamos cookies propias y de terceros para brindarte un mejor servicio, y facilitarte la utilización de nuestra web. Al navegar en esta web aceptas que las usemos.
    <a href="#Modal_Cookies"  data-toggle="modal">M&aacute;s Informaci&oacute;n</a>&nbsp;
    <button type="button" class="btn btn-primary btn-sm acceptcookies" aria-label="Close">
       Aceptar
    </button>
</div>
<!-- END Bootstrap Cookie Alert -->


<div class="jumbotron bottom jumbotron-fluid text-center">
  <p>Mutua MMT Seguros</p>
</div>


<!-- Política de Cookies -->
<div class="modal" id="Modal_Cookies">
  <div class="modal-dialog">
    <div class="modal-content">


      @foreach ($cookiesPolicy as $cookie)
     
     

      <!-- Modal Header -->
      <div class="modal-header">
        <h6 class="modal-title">{!! $cookie->title !!}</h6>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        {!!  $cookie->epigraph !!} 
      </div>

      @endforeach
    </div>
  </div>
</div>

<!-- End Politica de Cookies -->

<!-- Modal MMT -->
<div class="modal" id="Modal_MMT">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h6 class="modal-title">{!! $policyMain->title !!}</h6>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
{!! $policyMain->epigraph !!}
      
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <!-- button type="button" class="btn btn-danger" data-dismiss="modal">Close</button-->
      </div>

    </div>
  </div>
</div>

<!-- Modal Adicional -->
<div class="modal" id="Modal_adicional">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h6 class="modal-title">{!! $policyAdd->title !!}</h6>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        {!! $policyAdd->epigraph !!}
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <!-- button type="button" class="btn btn-danger" data-dismiss="modal">Close</button-->
      </div>

    </div>
  </div>
</div>
<script type="text/javascript">

  $(document).ready(function(){
      //$('#div_errors').empty();
      jQuery.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });
      /*
      $('#email').on('blur',function(e){
         
          $.ajax({
              url: "{{ url('/check-email') }}",
              headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
              method: 'post',
              data: {
                  email: $('#email').val(),
                  client:'mmt'
              },
              success: function(data) {
                //jQuery('.alert').alert('close');
                  $('#div_errors').empty();
                  $.each(data.errors, function(key, value){
                      $('.alert-danger').show();
                      $('.alert-danger').append('<p>'+value+'</p>');
                  });
                  jQuery('#validEmail').val(data.validEmail);
                  if (data.validEmail==1) {
                    $('#div_errors').empty();
                    jQuery('.alert').alert('close');
                  }
              }
          });
      });
      */
     // jQuery('.alert').alert();
      //jQuery('#btnAlertSuccessClose').click(function(e){
       //   e.preventDefault();
        //  jQuery('.alert').alert('close');
      //});
  });
</script>

<script src="{{  asset('js/cookiealert.js') }}"></script>

</body>
</html>