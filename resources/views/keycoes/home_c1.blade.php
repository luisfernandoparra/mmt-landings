
<!doctype html>
<html class="no-js" lang="es">
  <head>
    <meta charset="utf-8" />
   


   
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta charset="utf-8">
    <meta name="_token" content="{{ csrf_token() }}" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="Experto en Mantenimiento IT: Innovación Tecnológica. Consultoría para optimizar servicios y ahorro">
    <meta name="keywords" content="IT,consultoría,innovacion,tecnología,avanzar,mantenimiento,soporte,cloud,data,despliegue,infraestructura,desarrollo,aplicaciones,security,comunicaciones,cloud data,cloud security">
    <meta name="copyright" content="Keycoes: Innovacion y tecnología">
    <meta property="og:type" content="website" />
      <meta property="og:title" content="Keycoes Innovación y Tecnología">
      <meta property="og:site_name" content="Keycoes Innovación y Tecnología">
      <meta property="og:url" content="https://keycoes.e-retaildata.com">
      <meta property="og:description" content="Keycoes Experto en Mantenimiento IT: Innovación Tecnológica. Consultoría para optimizar servicios y ahorro">



    <title>Keycoes Innovación y Tecnología</title>


    <link rel="stylesheet" href="{{ asset('css/keycoes/campaign_1/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/keycoes/campaign_1/keycoes.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/keycoes/campaign_1/bootstrap.css') }}">

    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('imgs/keycoes/campaign_1/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('imgs/keycoes/campaign_1/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('imgs/keycoes/campaign_1/favicon-16x16.png') }}"> 
    <link rel="mask-icon" href="{{ asset('imgs/keycoes/campaign_1/safari-pinned-tab.svg') }} " color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="{{ asset('css/keycoes/campaign_1/cookiealert.css') }}">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

  <!-- Global site tag (gtag.js) - Google Ads: 950596229 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-950596229"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-950596229');
</script>
 

  </head>
  <body>
    <!-- Pixel de Display -->
<img src="https://eretail.uinterbox.com/tracking/cnv?org=1619&evt=3783"  height="1" width="1"/>
    <div class="campaign-pages">
    	<div class="container">
    		<header class="margin-bottom-20 margin-top-20">
    			<div class="columns is-mobile">
    				<div class="column">
    					<a href="http://www.keycoes.es/" target="_blank" class="logo"><img src="{{ asset('imgs/keycoes/campaign_1/logo_keycoes.png') }}" width="170" alt="Keycoes logo" alt="Keycoes"></a>
    				</div>
    				<!-- div class="column campaign-pages-login is-3">
    					<a href="" class="campaign-pages-login-link">Login</a>
    				</div -->
    			</div>
    		</header>
    		<div class="columns is-gapless">
    			<div class="column is-8" style="background-color: #05c2df;">
    				<div class="campaign-pages-hero">
    					<img src="{{ asset('imgs/keycoes/campaign_1/keycoes_landing.jpg') }}" class="especial_pg" alt="Keycoes">
                        <img src="{{ asset('imgs/keycoes/campaign_1/keycoes_landing_med.jpg') }}" class="especial_med" alt="Keycoes">
    				</div>


                </div>



    			<div class="column is-4" style="background-color: #05c2df;">
    				<div class="contact-form">
    					<div class="contact-form-quotes">

    						<div class="contact-form-quotes-callout"><strong>Descubre c&oacute;mo podemos ayudarte.</strong></div>

    					</div>
    					<div class="contact-form-body">
    							<form data-forms="floating-label" class="ml-form" name='forms' id='forms' method="POST">
                                    {{  csrf_field() }}
                                    <input type="hidden" id="id_source" name="id_source" value="{{ $source }}" />
      <input type="hidden" id="id_site" name="id_site" value="3" />
      <input type="hidden" id="validEmail" name="validEmail" value="0" />
    								<h3 class="contact-form-title">Cont&aacute;ctanos.</h3>
                                    <p class="contact-form-requiredtext"><i>Todos los campos son requeridos.</i></p>
                                    

                                    <div class="alert alert-danger alert-dismissible fade show" style="display: @if($errors->any()) block @else none @endif" role="alert">
                                        <div id="div_errors">
                                        @if($errors->any())
                                            @foreach ($errors->all() as $error)
                                                <div>{{ $error }}</div>
                                            @endforeach
                                        @endif
                                    </div>
                                        <button type="button"  class="close" data-dismiss="alert" aria-label="Close" onclick="jQuery('.alert').alert('close');">
                                                <span aria-hidden="true">&times;</span>
                                              </button>
                                    </div>



    								<fieldset class="ml-form-field">
                                        <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">

    							    <label>
                                        <input type="text" value='{{old('name')}}' id="name" name="name"  onkeyup="this.setAttribute('value', this.value);this.setAttribute('required','required');" required>
    							      
    							      <span>Nombre</span>
                                    </label>
                                        </div>
    							  </fieldset>
                                  <fieldset class="ml-form-field">
                                  <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">

    							    <label>
                                        <input type="email" value='{{old('email')}}' id="email" name="email"  onkeyup="this.setAttribute('value', this.value);this.setAttribute('required','required');" required>
    							      
    							      <span>E-mail</span>
                                    </label>
                                  </div>
    							  </fieldset>
    							  <fieldset class="ml-form-field">
                                    <div class="form-group {{ $errors->has('company') ? ' has-error' : '' }}">

    							    <label>
                                        <input type="text" onkeyup="this.setAttribute('value', this.value);this.setAttribute('required','required');" value='{{old('company')}}' id="company" name="company" required>
    							      
    							      <span>Empresa</span>
                                    </label>
                                    </div>
    							  </fieldset>


                                  <fieldset class="ml-form-field ml-form-field-checkboxes">
                                    <div class="ml-form-field-checkboxes-group form-group form-check {{ $errors->has('legalpolicy') ? ' has-error' : '' }}">
                                      <input id="legalpolicy" name="legalpolicy" value="1" required type="checkbox">
                                      <label for="legalpolicy">He leido y acepto la <a href="#Modal_keycoes" data-toggle="modal">pol&iacute;tica de privacidad.</a></label>
                                    </div>

                                    </fieldset>

                                    <div class="align-right">
                                        <button class="ml-button-primary" type="submit"  id="btnSubmit" name="btnSubmit">Enviar</button>
                                    </div>

    							</form>
    					</div>
    				</div>

    			</div>
    		</div>




    		<footer>

    			<div class="footer-legal" align="center">
    				<a href="http://www.keycoes.es/" target="_blank"><img src="{{ asset('imgs/keycoes/campaign_1/logo_keycoes.png') }}" width="140" class="footerlogo" style="position: relative; top: 0px;left: 0px;padding-bottom: 40px"></a>
    				<p>La Pol&iacute;tica de Protecci&oacute;n de Datos de KEYCOES COMUNICACIONES S.L. descansa en el principio de responsabilidad proactiva, seg&uacute;n el cual el Responsable del tratamiento es responsable del cumplimiento del marco normativo y jurisprudencial, siendo capaz de demostrarlo ante las autoridades de control correspondientes.</p>


    				<span class="footerlinks">
                        <a href="#Modal_keycoes" data-toggle="modal">Pol&iacute;tica de privacidad</a>

    				</span>
    				<p></p><p>&copy; 2020 Keycoes - Todos los derechos reservados.</p>
    			</div>
    		</footer>
    	</div>
    </div>
    <!-- START Bootstrap-Cookie-Alert -->
<div class="alert text-center cookiealert" role="alert">
    <b>&iquest;Te gustan las Cookies?</b> Utilizamos cookies propias y de terceros para brindarte un mejor servicio, y facilitarte la utilizaci&oacute;n de nuestra web. Al navegar en esta web aceptas que las usemos.
    <a href="#Modal_Cookies"  data-toggle="modal">M&aacute;s Informaci&oacute;n</a>
    <button type="button" class="btn btn-primary btn-sm acceptcookies" aria-label="Close">
       Aceptar
    </button>
</div>
<!-- END Bootstrap Cookie Alert -->


<!-- Modal MMT -->
<div class="modal" id="Modal_keycoes">
  <div class="modal-dialog">
    <div class="modal-content">


        @foreach ($privatePolicies as $policy)
        <!-- Modal Header -->
        <div class="modal-header">
          <h6 class="modal-title">{!! $policy->title !!}</h6>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
  {!! $policy->epigraph !!}
        
        </div>
  
        <!-- Modal footer -->
        <div class="modal-footer">
          <!-- button type="button" class="btn btn-danger" data-dismiss="modal">Close</button-->
        </div>
        @endforeach

      

     

    </div>
  </div>
</div>




<!-- Pol&iacute;tica de Cookies -->
<div class="modal" id="Modal_Cookies">
  <div class="modal-dialog" style="max-width:800px !important;">
    <div class="modal-content">





      <!-- Modal Header -->
      <div class="modal-header">
        <img src="{{ asset('imgs/keycoes/campaign_1/logo_keycoes.png') }}" width="160" alt="Keycoes Logo">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
                    <div style="width:90%; margin:auto;">
                                <div class="destaca" style="padding:25px;"> <h1 style="text-align: center;">Pol&iacute;tica de cookies</h1></div><p></p>
                                <p style="text-align: center;"><span class="content-snippet"><b></b></span></p>

                                @foreach ($cookiesPolicy as $cookie)
                                {!!  $cookie->epigraph !!} 
                                @endforeach

                              
                    </div>

        <footer>

          <div class="footer-legal">
            <img src="{{ asset('imgs/keycoes/campaign_1/logo_keycoes.png') }}" width="110" class="footerlogo"> <p></p>
            <p>&copy; 2020 Keycoes - Todos los derechos reservados.</p>
          </div>
        </footer>



      </div>

          </div>
  </div>
</div>



<script type="text/javascript">

    $(document).ready(function(){
        //$('#div_errors').empty();
        jQuery.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
       
    });
  </script>


    <!--
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
    <script src="{{ asset('js/keycoes/campaign_1/cookiealert.js') }}"></script>
    <script src="{{ asset('js/keycoes/campaign_1/app.js') }}"></script>
  </body>
</html>
