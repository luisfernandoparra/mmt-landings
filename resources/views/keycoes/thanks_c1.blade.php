
<!doctype html>
<html class="no-js" lang="es">
  <head>
    <meta charset="utf-8" />


   
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta charset="utf-8">
    <meta name="_token" content="{{ csrf_token() }}" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="Experto en Mantenimiento IT: Innovación Tecnológica. Consultoría para optimizar servicios y ahorro">
    <meta name="keywords" content="IT,consultoría,innovacion,tecnología,avanzar,mantenimiento,soporte,cloud,data,despliegue,infraestructura,desarrollo,aplicaciones,security,comunicaciones,cloud data,cloud security">
    <meta name="copyright" content="Keycoes: Innovacion y tecnología">
    <meta property="og:type" content="website" />
      <meta property="og:title" content="Keycoes Innovación y Tecnología">
      <meta property="og:site_name" content="Keycoes Innovación y Tecnología">
      <meta property="og:url" content="https://keycoes.e-retaildata.com">
      <meta property="og:description" content="Keycoes Experto en Mantenimiento IT: Innovación Tecnológica. Consultoría para optimizar servicios y ahorro">

      <link rel="stylesheet" href="{{ asset('css/keycoes/campaign_1/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/keycoes/campaign_1/keycoes.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/keycoes/campaign_1/bootstrap.css') }}">

      <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('imgs/keycoes/campaign_1/apple-touch-icon.png') }}">
      <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('imgs/keycoes/campaign_1/favicon-32x32.png') }}">
      <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('imgs/keycoes/campaign_1/favicon-16x16.png') }}"> 
      <link rel="mask-icon" href="{{ asset('imgs/keycoes/campaign_1/safari-pinned-tab.svg') }} " color="#5bbad5">
      <meta name="msapplication-TileColor" content="#da532c">
      <meta name="theme-color" content="#ffffff">

     <!--- <link rel="stylesheet" href="{{ asset('css/keycoes/campaign_1/cookiealert.css') }}"> -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <!-- Global site tag (gtag.js) - Google Ads: 950596229 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-950596229"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-950596229');
</script>
 

  </head>
  <body>
    @if (!empty($id) && $result==1)
    <img src="https://eretail.uinterbox.com/tracking/cnv?org=1619&evt=3784&ue1={{ $email }}&ue2={{ $name }}&ue3={{ $company }}" height="1" width="1">
    <!-- Event snippet for LEads Eretail conversion page
      In your html page, add the snippet and call gtag_report_conversion when someone clicks on the chosen link or button. -->
      <script>
      function gtag_report_conversion(url) {
        var callback = function () {
          if (typeof(url) != 'undefined') {
            window.location = url;
          }
        };
        gtag('event', 'conversion', {
            'send_to': 'AW-950596229/-hPwCIGH8eEBEIXlo8UD',
            'event_callback': callback
        });
        return false;
      }
      </script>
    @endif
    <div class="campaign-pages">
    	<div class="container">
    		<header class="margin-bottom-20 margin-top-20">
    			<div class="columns is-mobile">
    				<div class="column">
    					<a href="http://www.keycoes.es/" target="_blank" class="logo"><img src="{{ asset('imgs/keycoes/campaign_1/logo_keycoes.png') }}" width="170" alt="Keycoes logo" alt="Keycoes"></a>
    				</div>
    				<!-- div class="column campaign-pages-login is-3">
    					<a href="" class="campaign-pages-login-link">Login</a>
    				</div -->
    			</div>
    		</header>

    		<div class="columns is-gapless">
    			<div class="column is-12" align="center">

    				<div class="campaign-pages-main" style="background-color: #05c2df;">
    					<h1 style="color:#fff; display:inline">Gracias.</h1>
                        <h2 style="font-size:20px;color:#fff;padding-top:18px;"><strong>Hemos recibido tu solicitud.<br>En breve, te contactaremos para darte m&aacute;s informaci&oacute;n.</strong></h2>
                    </div>
                    <div class="campaign-pages-hero" style="background-color: #05c2df;">
    					<a href="http://www.keycoes.es/" target="_blank"><img src="{{ asset('imgs/keycoes/campaign_1/gracias_keycoes.jpg') }}" style="border:0;" border="0" alt="Keycoes"></a>

    				</div>

                </div>

    		</div>




    		<footer>

    			<div class="footer-legal" align="center">
    				
    				<p>La Pol&iacute;tica de Protecci&oacute;n de Datos de KEYCOES COMUNICACIONES S.L. descansa en el principio de responsabilidad proactiva, seg&uacute;n el cual el Responsable del tratamiento es responsable del cumplimiento del marco normativo y jurisprudencial, siendo capaz de demostrarlo ante las autoridades de control correspondientes.</p>


    				<span class="footerlinks">
                        <a href="#Modal_keycoes" data-toggle="modal">Pol&iacute;tica de privacidad</a>

    				</span>
    				<p></p><p>&copy; 2020 Keycoes - Todos los derechos reservados.</p>
    			</div>
    		</footer>
    	</div>
    </div>




<!-- Pol&iacute;tica de Cookies -->
<div class="modal" id="Modal_Cookies" style="display: none;">

  
</div>

<div class="modal" id="Modal_keycoes">
    <div class="modal-dialog">
      <div class="modal-content">
  
  
          @foreach ($privatePolicies as $policy)
          <!-- Modal Header -->
          <div class="modal-header">
            <h6 class="modal-title">{!! $policy->title !!}</h6>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <!-- Modal body -->
          <div class="modal-body">
    {!! $policy->epigraph !!}
          
          </div>
    
          <!-- Modal footer -->
          <div class="modal-footer">
            <!-- button type="button" class="btn btn-danger" data-dismiss="modal">Close</button-->
          </div>
          @endforeach
  
  
        
  
       
  
      </div>
    </div>
  </div>
  


  <script type="text/javascript">

    $(document).ready(function(){
        //$('#div_errors').empty();
        jQuery.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
       
    });
  </script>



  
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
   <!---  <script src="{{ asset('js/keycoes/campaign_1/cookiealert.js') }}"></script> -->
    <script src="{{ asset('js/keycoes/campaign_1/app.js') }}"></script>
  </body>
</html>
