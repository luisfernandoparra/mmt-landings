<!doctype html>
<html class="no-js" lang="es">
  <head>
    
   
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta charset="utf-8">
    <meta name="_token" content="{{ csrf_token() }}" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="Experto en Mantenimiento IT: Innovación Tecnológica. Consultoría para optimizar servicios y ahorro">
    <meta name="keywords" content="IT,consultoría,innovacion,tecnología,avanzar,mantenimiento,soporte,cloud,data,despliegue,infraestructura,desarrollo,aplicaciones,security,comunicaciones,cloud data,cloud security">
    <meta name="copyright" content="Keycoes: Innovacion y tecnología">
    <meta property="og:type" content="website" />
      <meta property="og:title" content="Keycoes Innovación y Tecnología">
      <meta property="og:site_name" content="Keycoes Innovación y Tecnología">
      <meta property="og:url" content="https://keycoes.e-retaildata.com">
      <meta property="og:description" content="Keycoes Experto en Mantenimiento IT: Innovación Tecnológica. Consultoría para optimizar servicios y ahorro">



    <title>Keycoes Innovación y Tecnología</title>

    <link rel="stylesheet" href="{{ asset('css/metlife/campaign_1/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/metlife/campaign_1/metlife-global.min.css') }}">
    <link rel="stylesheet" href="{{  asset('css/metlife/campaign_1/bootstrap.css')  }}">
    <link rel="shortcut icon" href="{{ asset('imgs/metlife/campaign_1/favicon.ico') }}" />


<link rel="stylesheet" href="{{ asset('css/metlife/campaign_1/cookiealert.css') }}">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

  </head>
  <body>

    <div class="campaign-pages">
    	<div class="container">
    		<header class="margin-bottom-20 margin-top-20">
    			<div class="columns is-mobile">
    				<div class="column">
    					<a href="" target="_blank" class="logo"><img src="{{ asset('imgs/metlife/campaign_1/metlife-logo.png') }}" alt="Metlife Logo"></a>
    				</div>
    				<!-- div class="column campaign-pages-login is-3">
    					<a href="" class="campaign-pages-login-link">Login</a>
    				</div -->
    			</div>
    		</header>
    		<div class="columns is-gapless">
    			<div class="column is-8">
    				<div class="campaign-pages-hero" style="background:#0090da">
    					<img src="{{ asset('imgs/metlife/campaign_1/metlife-toplanding.png') }}" alt="Description of campaign">
    				</div>
    				<div class="campaign-pages-main" style="background:#0090da">
    				  <div class="tit_campaign" style="color:#fff; display:inline">Plan Dental Plus, desde 11,45&euro;* al mes</div>
                        <span style="font-size:12px; color:#fff">*(imp. incl.) </span>

                <div class="subtit_campaign"><strong>Hasta un 35% de descuento en servicios dentales.</strong></div>
                        <p><b>Seguro de indemnizaci&oacute;n por hospitalizaci&oacute;n por accidente.</b><br>
                           <b>Adem&aacute;s incluye un servicio de asistencia dental en copago con descuentos.</b>
                        </p>

                        <!-- a href="#" class="ml-button-primary-dark">> Nosotros de llamamos</a -->


                    </div>




                    <div class="campaign-pages-main mov" style="background:#fff">

                        <h3><strong>Ventajas de Plan Dental Plus </strong></h3>


              	    	<div class="tile-0 subcategory-tile product-module__large flex-container bg-white">
                          <div class="productmodule_section1">
                          <img src="{{ asset('imgs/metlife/campaign_1/metlife_sencillo.jpg') }}"  style="width:100%" alt="Sencillo">
                          </div>
                      	  <div class="productmodule_section2">
                      	    <div class="product-module__top" style="min-height: 153px;">
                      	        <h3 style="min-height: 40px;">Contrataci&oacute;n sencilla</h3>
                                <div class="product-module__list">
                                        <ul>Mediante una simple llamada de tel&eacute;fono.</ul>
                                </div>
                              </div>
                          </div>
                        </div>

                        <div class="tile-0 subcategory-tile product-module__large flex-container bg-white">
                          <div class="productmodule_section1">
                          <img src="{{ asset('imgs/metlife/campaign_1/metlife_hospital.jpg') }}"  style="width:100%" alt="Renta diaria en caso de hospitalizaci&oacute;n">
                          </div>
                    	  <div class="productmodule_section2">
                    	    <div class="product-module__top" style="min-height: 153px;">
                    	        <h3 style="min-height: 40px;">Hasta 300&euro;/d&iacute;a hospitalizado</h3>
                                    <div class="product-module__list">
                                    <ul>Renta diaria en caso de hospitalizaci&oacute;n por accidente.</ul>
                                    </div>
                            </div>
                          </div>
                      </div>

                      <div class="tile-0 subcategory-tile product-module__large flex-container bg-white">
                          <div class="productmodule_section1">
                          <img src="{{ asset('imgs/metlife/campaign_1/metlife_nino.jpg') }}"  style="width:100%" alt="Hasta 5 hijos">
                          </div>
                  	      <div class="productmodule_section2">
                  	        <div class="product-module__top" style="min-height: 153px;">
                  	            <h3 style="min-height: 40px;">Cobertura hasta 5 hijos</h3>
                                  <div class="product-module__list">
                                  <ul>Opci&oacute;n de cobertura familiar hasta 5 hijos.</ul>
                                  </div>
                            </div>
                          </div>
                      </div>


                </div>





            <div class="campaign-pages-main mov" style="background:#fff">


                <div class="tile-0 subcategory-tile product-module__large flex-container bg-white">
                  <div class="productmodule_section1">
                  <img src="{{ asset('imgs/metlife/campaign_1/metlife_servicios.jpg') }}"  style="width:100%" alt="Servicios dentales gratuitos">
                  </div>
              	<div class="productmodule_section2">
              	    <div class="product-module__top" style="min-height: 153px;">
              	        <h3 style="min-height: 40px;">Servicios dentales gratuitos</h3>

              	            <div class="product-module__list"><ul>
                              M&aacute;s de 25 servicios dentales gratuitos.
                              </ul>
                              </div>
                      </div>
                  </div>
                  </div>



                  <div class="tile-0 subcategory-tile product-module__large flex-container bg-white">
                  <div class="productmodule_section1">
                  <img src="{{ asset('imgs/metlife/campaign_1/metlife_ahorro.jpg') }}"  style="width:100%" alt="Renta diaria en caso de hospitalizaci&oacute;n">
                  </div>
              	<div class="productmodule_section2">
              	    <div class="product-module__top" style="min-height: 153px;">
              	        <h3 style="min-height: 40px;">Ahorra un 35% en tu dentista</h3>

              	            <div class="product-module__list">
                            <ul>Hasta un 35% de ahorro en servicios dentales.</ul>
                              </div>
                      </div>
                  </div>
                  </div>

                  <div class="tile-0 subcategory-tile product-module__large flex-container bg-white">
                        <div class="productmodule_section1">
                        <img src="{{ asset('imgs/metlife/campaign_1/metlife_elige.jpg') }}"  style="width:100%" alt="Hasta 5 hijos">
                        </div>
                	    <div class="productmodule_section2">
                    	    <div class="product-module__top" style="min-height: 153px;">
                    	        <h3 style="min-height: 40px;">Elige tu cl&iacute;nica y especialista</h3>

                                    <div class="product-module__list"><ul>
                                    2.000 especialistas y 1.600 cl&iacute;nicas
                                    </ul>
                                    </div>
                            </div>
                        </div>
                  </div>

                </div>


                </div>



    			<div class="column is-4">
    				<div class="contact-form">
    					<div class="contact-form-quotes">
    						<div>Te llamamos gratis.</div>
    						<div class="contact-form-quotes-callout"><strong>Deja que te asesoremos sin compromiso</strong></div>

    					</div>
    					<div class="contact-form-body">
    							<form data-forms="floating-label" class="ml-form" name='forms' id='forms' method="POST">
                                    {{  csrf_field() }}
                                    <input type="hidden" id="id_source" name="id_source" value="{{ $source }}" />
      <input type="hidden" id="id_site" name="id_site" value="3" />
      <input type="hidden" id="validEmail" name="validEmail" value="0" />
    								<h3 class="contact-form-title">Cont&aacute;ctanos.</h3>
                                    <p class="contact-form-requiredtext"><i>Todos los campos son requeridos.</i></p>
                                    

                                    <div class="alert alert-danger alert-dismissible fade show" style="display: @if($errors->any()) block @else none @endif" role="alert">
                                        <div id="div_errors">
                                        @if($errors->any())
                                            @foreach ($errors->all() as $error)
                                                <div>{{ $error }}</div>
                                            @endforeach
                                        @endif
                                    </div>
                                        <button type="button"  class="close" data-dismiss="alert" aria-label="Close" onclick="jQuery('.alert').alert('close');">
                                                <span aria-hidden="true">&times;</span>
                                              </button>
                                    </div>


                                    
    								<fieldset class="ml-form-field">
                                        <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
    							    <label for="name">
    							      <input type="text" value='{{old('name')}}' id="name" name="name"  onkeyup="this.setAttribute('value', this.value);this.setAttribute('required','required');">
    							      <span>Nombre</span>
                                    </label>
                                </div>
                                  </fieldset>



                                  <fieldset class="ml-form-field">
                                  <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email">
                                    <input type="email" value='{{old('email')}}' id="email" name="email"  onkeyup="this.setAttribute('value', this.value);this.setAttribute('required','required');">
                                        <span>Email</span>
                                    </label>
                                </div>
                                  </fieldset>

                                    
    							  <fieldset class="ml-form-field">
                                    <div class="form-group {{ $errors->has('company') ? ' has-error' : '' }}">
    							    <label for="company">
    							      <input type="text" onkeyup="this.setAttribute('value', this.value);this.setAttribute('required','required');" value='{{old('company')}}' id="company" name="company">
                                      <span>Compa&ntilde;&iacute;a</span>
                                    </label>
                                    </div>
    							  </fieldset>
                                 
                              
                                    <fieldset class="ml-form-field ml-form-field-checkboxes">
                                    <div class="ml-form-field-checkboxes-group form-group form-check {{ $errors->has('legalpolicy') ? ' has-error' : '' }}">
                                      <input id="legalpolicy" name="legalpolicy" value="1" required type="checkbox">
                                      <label for="legalpolicy">He leido y acepto la <a href="#Modal_keycoes" data-toggle="modal">pol&iacute;tica de privacidad.</a></label>
                                    </div>

                                    </fieldset>

    							  <div class="align-right">
    							  	<button class="ml-button-primary" type="submit"  id="btnSubmit" name="btnSubmit">Enviar</button>
    							  </div>

    							</form>
    					</div>

                        <p></p>
                        <div class="destaca">
                            <h1>Plan Dental Plus</h1>
                            <h2>&iquest;Por qu&eacute; conformarse con un seguro dental cl&aacute;sico? </h2>
                            <p><strong>Disponga de los beneficios que le ofrece el Plan Dental Plus desde 11,45&euro;* al mes</strong> *(imp. incl.)</p><br>
                            <p>Un seguro &uacute;nico que te ayudar&aacute; en esos momentos dif&iacute;ciles tras un accidente; ayuda para cuidar de tus hijos, pagar transporte extra o hacer frente a facturas si eres un aut&oacute;nomo&hellip;</p>
                            </div>
    				</div>
                    <img src="{{ asset('imgs/metlife/campaign_1/muela.png') }}" alt="Metlife">


<!--- Duplicamos para mobile START -->
                    <div class="campaign-pages-main desk" style="background:#fff">

                      <h3><strong>Ventajas de Plan Dental Plus </strong></h3>


                    <div class="tile-0 subcategory-tile product-module__large flex-container bg-white">
                        <div class="productmodule_section1">
                        <img src="{{ asset('imgs/metlife/campaign_1/metlife_sencillo.jpg') }}"  style="width:100%" alt="Sencillo">
                        </div>
                        <div class="productmodule_section2">
                          <div class="product-module__top" style="min-height: 153px;">
                              <h3 style="min-height: 40px;">Contrataci&oacute;n sencilla</h3>
                              <div class="product-module__list">
                                      <ul>Mediante una simple llamada de tel&eacute;fono.</ul>
                              </div>
                            </div>
                        </div>
                      </div>

                      <div class="tile-0 subcategory-tile product-module__large flex-container bg-white">
                        <div class="productmodule_section1">
                        <img src="{{ asset('imgs/metlife/campaign_1/metlife_hospital.jpg') }}"  style="width:100%" alt="Renta diaria en caso de hospitalizaci&oacute;n">
                        </div>
                      <div class="productmodule_section2">
                        <div class="product-module__top" style="min-height: 153px;">
                            <h3 style="min-height: 40px;">Hasta 300&euro;/d&iacute;a hospitalizado</h3>
                                  <div class="product-module__list">
                                  <ul>Renta diaria en caso de hospitalizaci&oacute;n por accidente.</ul>
                                  </div>
                          </div>
                        </div>
                    </div>

                    <div class="tile-0 subcategory-tile product-module__large flex-container bg-white">
                        <div class="productmodule_section1">
                        <img src="{{ asset('imgs/metlife/campaign_1/metlife_nino.jpg') }}"  style="width:100%" alt="Hasta 5 hijos">
                        </div>
                        <div class="productmodule_section2">
                          <div class="product-module__top" style="min-height: 153px;">
                              <h3 style="min-height: 40px;">Cobertura hasta 5 hijos</h3>
                                <div class="product-module__list">
                                <ul>Opci&oacute;n de cobertura familiar hasta 5 hijos.</ul>
                                </div>
                          </div>
                        </div>
                    </div>


              </div>





          <div class="campaign-pages-main desk" style="background:#fff">


              <div class="tile-0 subcategory-tile product-module__large flex-container bg-white">
                <div class="productmodule_section1">
                <img src="{{ asset('imgs/metlife/campaign_1/metlife_servicios.jpg') }}"  style="width:100%" alt="Servicios dentales gratuitos">
                </div>
              <div class="productmodule_section2">
                  <div class="product-module__top" style="min-height: 153px;">
                      <h3 style="min-height: 40px;">Servicios dentales gratuitos</h3>

                          <div class="product-module__list"><ul>
                            M&aacute;s de 25 servicios dentales gratuitos.
                            </ul>
                            </div>
                    </div>
                </div>
                </div>



                <div class="tile-0 subcategory-tile product-module__large flex-container bg-white">
                <div class="productmodule_section1">
                <img src="{{ asset('imgs/metlife/campaign_1/metlife_ahorro.jpg') }}"  style="width:100%" alt="Renta diaria en caso de hospitalizaci&oacute;n">
                </div>
              <div class="productmodule_section2">
                  <div class="product-module__top" style="min-height: 153px;">
                      <h3 style="min-height: 40px;">Ahorra un 35% en tu dentista</h3>

                          <div class="product-module__list">
                          <ul>Hasta un 35% de ahorro en servicios dentales.</ul>
                            </div>
                    </div>
                </div>
                </div>

                <div class="tile-0 subcategory-tile product-module__large flex-container bg-white">
                      <div class="productmodule_section1">
                      <img src="{{ asset('imgs/metlife/campaign_1/metlife_elige.jpg') }}"  style="width:100%" alt="Hasta 5 hijos">
                      </div>
                    <div class="productmodule_section2">
                        <div class="product-module__top" style="min-height: 153px;">
                            <h3 style="min-height: 40px;">Elige tu cl&iacute;nica y especialista</h3>

                                  <div class="product-module__list"><ul>
                                  2.000 especialistas y 1.600 cl&iacute;nicas
                                  </ul>
                                  </div>
                          </div>
                      </div>
                </div>

              </div>

              <!--- Duplicamos para mobile END -->





    			</div>
    		</div>




    		<footer>
    			<div class="colorbar columns">
    				<div class="metlife-blue column is-8 column"></div>
    				<div class="metlife-dark-blue is-3 column"></div>
    				<div class="metlife-green is-1 column"></div>
    			</div>
    			<div class="footer-legal">
    				<img src="{{ asset('imgs/metlife/campaign_1/metlife-footerlogo.png') }}" class="footerlogo"> <p></p>
    				<p>MetLife Europe d.a.c. es una sociedad con actividad designada por acciones registrada en Irlanda, con domicilio social en "20 on Hatch, Lower Hatch Street, Dublin 2, Irlanda" y con n&uacute;mero de registro 415123. Los administradores de la sociedad son Brenda Dunne (irlandesa), &Eacute;ilish Finan (irlandesa), Michael Hatzidimitriou (Griego), Nick Hayter (Brit&aacute;nico), Ruair&iacute; O&rsquo;Flynn, Dirk Ostijn (Belga), Miriam Sweeney (irlandesa), Mario Valdes (Mexicano). MetLife Europe d.a.c. Sucursal en Espa&ntilde;a, con domicilio social en Madrid, Avenida de los Toreros, n&ordm;3 (CP 28028) se encuentra inscrita en el Registro Mercantil de Madrid (Tomo 30.276, Folio 192, Secci&oacute;n 8&ordf;, Hoja M-544907 e Inscripci&oacute;n 1&ordf;), provista de CIF n&ordm; W-0072536-F, y en el Registro Administrativo de la Direcci&oacute;n General de Seguros y Fondos de Pensiones con el n&ordm; E-0208, estando autorizada para actuar en los ramos de Vida, Accidentes y Enfermedad. La compa&ntilde;&iacute;a est&aacute; regulada por el Central Bank of Ireland, todo ello sin perjuicio de las facultades atribuidas a la Direcci&oacute;n General de Seguros y Fondos de Pensiones para la defensa de los consumidores. De acuerdo a lo establecido en la normativa vigente en materia de seguros, a esta entidad no le ser&aacute; de aplicaci&oacute;n la normativa espa&ntilde;ola de liquidaci&oacute;n de entidades aseguradoras.</p>


    				<div class="footerlinks">
                        <a href="https://www.metlife.es/politica-privacidad/" target="blank">Pol&iacute;tica de privacidad</a>
    					
    				</div>
    				<p>&copy; 2020 MetLife - Todos los derechos reservados.</p>
    			</div>
    		</footer>
    	</div>
    </div>
    <!-- START Bootstrap-Cookie-Alert -->
<div class="alert text-center cookiealert" role="alert">
    <b>&iquest;Te gustan las Cookies?</b> Utilizamos cookies propias y de terceros para brindarte un mejor servicio, y facilitarte la utilizaci&oacute;n de nuestra web. Al navegar en esta web aceptas que las usemos.
    <a href="#Modal_Cookies"  data-toggle="modal">M&aacute;s Informaci&oacute;n</a>
    <button type="button" class="btn btn-primary btn-sm acceptcookies" aria-label="Close">
       Aceptar
    </button>
</div>
<!-- END Bootstrap Cookie Alert -->




<!-- Modal MMT -->
<div class="modal" id="Modal_keycoes">
  <div class="modal-dialog">
    <div class="modal-content">


        @foreach ($privatePolicies as $policy)
        <!-- Modal Header -->
        <div class="modal-header">
          <h6 class="modal-title">{!! $policy->title !!}</h6>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
  {!! $policy->epigraph !!}
        
        </div>
  
        <!-- Modal footer -->
        <div class="modal-footer">
          <!-- button type="button" class="btn btn-danger" data-dismiss="modal">Close</button-->
        </div>
        @endforeach


      

     

    </div>
  </div>
</div>


<!-- Pol&iacute;tica de Cookies -->
<div class="modal" id="Modal_Cookies">
  <div class="modal-dialog" style="max-width:800px !important;">
    <div class="modal-content">


        @foreach ($cookiesPolicy as $cookie)
        <!-- Modal Header -->
        <div class="modal-header">
          <img src="{{ asset('imgs/metlife/campaign_1/metlife-logo.png') }}" alt="Metlife Logo">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            {!!  $cookie->epigraph !!} 
            <footer>
                <div class="colorbar columns">
                  <div class="metlife-blue column is-8 column"></div>
                  <div class="metlife-dark-blue is-3 column"></div>
                  <div class="metlife-green is-1 column"></div>
                </div>
                <div class="footer-legal">
                  <img src="{{ asset('imgs/metlife/campaign_1/metlife-footerlogo.png') }}" class="footerlogo"> <p></p>
                  <p>&copy; 2020 MetLife - Todos los derechos reservados.</p>
                </div>
              </footer>
      
          </div>

        @endforeach


      


        


      </div>

          </div>
  </div>
</div>



<script type="text/javascript">

  $(document).ready(function(){
      //$('#div_errors').empty();
      jQuery.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });
     
  });
</script>




    <script src="{{ asset('js/metlife/campaign_1/cookiealert.js') }}"></script>
    <script src="{{ asset('js/metlife/campaign_1/app.js') }}"></script>
  </body>
</html>