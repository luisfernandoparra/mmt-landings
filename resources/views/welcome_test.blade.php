<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="_token" content="{{csrf_token()}}" />
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Captación</title>

        <!-- JS LIbraries -->
        <!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>


        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: left;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Form Test
                </div>
                <div class="alert alert-danger alert-dismissible fade show" style="display: @if($errors->any()) block @else none @endif" role="alert">
                    <div id="div_errors">
                    @if($errors->any())
                        @foreach ($errors->all() as $error)
                            <div>{{ $error }}</div>
                        @endforeach
                    @endif
                </div>
                    <button type="button"  class="close" data-dismiss="alert" aria-label="Close" onclick="jQuery('.alert').alert('close');">
                            <span aria-hidden="true">&times;</span>
                          </button>
                </div>
                @php
                    $message = (!empty($id)) ? $message : '';
                @endphp
                <div class="alert alert-success  alert-dismissible fade show"  style="display: @if (!empty($id)) block @else none @endif" role="alert">
                    {{ $message }}
                    <button type="button"  class="close" data-dismiss="alert" aria-label="Close" onclick="jQuery('.alert').alert('close');">
                            <span aria-hidden="true">&times;</span>
                          </button>
                </div>
                <form name='forms' id='forms' method='post' action="">
                        {{ csrf_field() }}
                    <input type="hidden" id="id_source" name="id_source" value="{{ $source }}" />
                    <input type="hidden" id="id_site" name="id_site" value="1" />
                    <input type="hidden" id="validEmail" name="validEmail" value="0" />

                    
                            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name" class="col-sm-2">Name</label>
                                    <input required type='text' id='name' name='name' value='{{old('name')}}'> <br/><small class="text-danger">@if($errors->has('name')){{$errors->get('name')[0]}} @endif</small>
                            </div>
                            <div class="form-group  {{ $errors->has('surname') ? ' has-error' : '' }}">
                                    <label for="surname" class="col-sm-2">Surname</label>
                                    <input required type='text' id='surname' name='surname' value='{{old('surname')}}' > <br/><small class="text-danger"> @if($errors->has('surname')) error: {{$errors->get('surname')[0]}} @endif</small>
                            </div>
                            <div class="form-row">
                                    <div class="form-group col-md-5 {{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label for="email" class="col-sm-2">Email</label>
                                            <input required type='email' id='email' name='email' value='{{old('email')}}'> <br/>
                                            <small class="text-danger">{{ $errors->first('email') }}</small> 
                                    </div>
                                    <div class="form-group col-md-5  {{ $errors->has('phone') ? ' has-error' : '' }}">
                                            <label for="phone" class="col-sm-2">Phone</label>
                                            <input required minlength="9" maxlength="9" type='number' id='phone' name='phone' value='{{old('phone')}}' > <br/><small class="text-danger">@if($errors->has('phone')) error: {{$errors->get('phone')[0]}} @endif</small>
                                    </div>
                    </div>
                    <div class="form-row">
                            <div class="form-group col-md-6  {{ $errors->has('id_province') ? ' has-error' : '' }}">
                                    @php 
                                    $id_province = ''; 
                                    if ($errors->any()):
                                    $id_province = old('id_province');
                                    
                                    
                                    endif;
        
        
                                    @endphp
                                    <label for="id_province" class="col-sm-2">Province</label>
                                    <select required id="id_province" name="id_province">
                                        <option value="">Select a Province</option>
                                            @foreach ($provinces as $province)
                                            <option value="{{ $province->id }}" @if ($province->id==$id_province) selected="selected" @endif>{{ $province->province }}</option>
                                                
                                            @endforeach
                                    </select><br/><small class="text-danger">@if($errors->has('id_province')) error: {{$errors->get('id_province')[0]}} @endif</small>
                            </div>
                            <div class="form-group col-md-4  {{ $errors->has('id_province') ? ' has-error' : '' }}">
                                    <label for="zip">zip</label>
                                    <input required minlength="4" maxlength="5" type='number' id='zip' name='zip' value='{{old('zip')}}' ><br/><small class="text-danger">@if($errors->has('zip')) error: {{$errors->get('zip')[0]}} @endif</small>
                            </div>

                    </div>
                <br><br>
                <input type="submit" id="btnSubmit" name="btnSubmit" value="Guardar" class="btn btn-outline-info">

                </form>

                <div class="links">
                    <a href="https://laravel.com/docs">Docs</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://blog.laravel.com">Blog</a>
                    <a href="https://nova.laravel.com">Nova</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div>
            </div>
        </div>

        <script type="text/javascript">

            $(document).ready(function(){
                //$('#div_errors').empty();
                jQuery.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                
                $('#email').on('blur',function(e){
                   
                    $.ajax({
                        url: "{{ url('/check-email') }}",
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        method: 'post',
                        data: {
                            email: $('#email').val()
                        },
                        success: function(data) {
                            $('#div_errors').empty();
                            $.each(data.errors, function(key, value){
                                $('.alert-danger').show();
                                $('.alert-danger').append('<p>'+value+'</p>');
                            });
                            jQuery('#validEmail').val(data.validEmail);
                        }
                    });
                });
               // jQuery('.alert').alert();
                //jQuery('#btnAlertSuccessClose').click(function(e){
                 //   e.preventDefault();
                  //  jQuery('.alert').alert('close');
                //});
            });
        </script>



    </body>
</html>
