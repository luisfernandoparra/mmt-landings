<?php

namespace App\Console\Commands\mmt;

use Illuminate\Console\Command;
//use DB;
//use Carbon\Carbon;
use GuzzleHttp\Client;
use App\miscClass\miscClass as AppMiscClass;

class SendData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'landings:mmt:senddata';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Recogerá todos los datos pendientes de ser enviados de MMT Seguros y los mandará vía WS al cliente, actualizando los datos recibidos o respuestas';

    /**
     * Constante que define el identificador de cliente de MMT
     *
     * @var integer
     */
    protected $client_id = 1;

    /**
     * Objeto clase que gestiona conexión y datos a gestionar.
     *
     * @var object
     */
    protected $clsMisc;
    /**
     * URL del WS a enviar. 
     * lo ponemos aquí por ser más fácil
     * Url de Test: https://test00.mmtseguros.es/produccion/lead
     * Url de Producción: https://www.mmtseguros.es/produccion/lead
     *
     * @var string
     */
    protected $url = "https://www.mmtseguros.es/produccion/lead";
    //protected $url = "http://test00.mmtseguros.es/produccion/lead";

    protected $url_fields = "?clave=869eb4ad02b9b046d83980f8795ef4bb&telefono=%d&nombre=%s&ape1=%s&ape2=%s&email=%s&fechainscripcion=%s&idregistro=%d&consentimiento=si&tipovivienda=%s&fechavencimiento=%s";
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->clsMisc = new AppMiscClass();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Process to be started');
        $data = $this->clsMisc->ws_getRegisters($this->client_id);
        /**
         * Columnas devueltas
         * 'id','name','email','phone','ip','add_info_1','add_info_2','b_accept_cookies','b_accept_legal_issues','created_at'
         */
        $bar = $this->output->createProgressBar(count($data));
        foreach ($data as $datum) {
            $id = $datum->id;
            $name = ucwords(trim($datum->name));
            $email = strtolower(trim($datum->email));
            $phone = trim($datum->phone);
            $ip = trim($datum->ip);
            $homeType = strtoupper(trim($datum->add_info_1));
            $dueDate = $this->clsMisc->dateReverse(trim($datum->add_info_2));
            $bCookies = $datum->b_accept_cookies;
            $bLegal = $datum->b_accept_legal_issues;
            $date = $datum->created_at;
            $dateTmp = explode(' ', $date);
            $dateNew = $this->clsMisc->dateReverse($dateTmp[0]) . ' ' . $dateTmp[1];

            $fieldsItem = sprintf($this->url_fields, $phone, $name, '', '', $email, $dateNew, $id, $homeType, $dueDate);
            /**
             * Insertamos registro en el log antes de mandar
             */
            $idLocal = $this->clsMisc->ws_setStartSent($id, 'GET', $this->url, $fieldsItem, '', '', true);
            //$this->warn($idLocal."\n");
           // continue;
            
            /**
             * Enviamos
             */
            
            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', $this->url . $fieldsItem);
            $statusCode = $response->getStatusCode();
            $header = [];
            foreach ($response->getHeaders() as $name => $values) {
                $header[] = $name . ": " . implode(", ", $values);
            }

            $resp = (string)$response->getBody();
            
            $this->info("Respuesta: ".$resp."\n");
            $resp = trim($resp);
            //$resp2 = $response->getBody()->getContents();
            //$this->info("Respuesta Content: ".$resp2."\n");
            $this->info($this->url . $fieldsItem."\n");
            $this->warn("Response: ".$resp."\n");

            unset($client);
/*
$resp = ($id<=2) ? 'OK' : 'NOK: pero qué pasa aquí';
$statusCode = 200;
$header=['hola'=>'adios'];
*/
            /**
             * Procesamos las respuestas
             */
            $b_status = (strpos(strtoupper($resp), 'NOK') === false);

            $this->clsMisc->ws_setEndSent($idLocal,$id,$b_status,$statusCode,$resp,$header);
            $bar->advance();
        }
        $this->warn('Terminamos el proceso'."\n");
        $bar->finish();
        $this->info("\n");
        $this->info("\n");
        $this->info("\n");
        $this->info("\n");
    }
}
