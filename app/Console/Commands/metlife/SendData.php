<?php

namespace App\Console\Commands\metlife;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use Carbon\Carbon;
use App\miscClass\miscClass as AppMiscClass;

class SendData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'landings:metlife:senddata';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Recogerá todos los datos pendientes de ser enviados de Metlife y los mandará vía WS al cliente, actualizando los datos recibidos o respuestas';
    /**
     * Constante que define el identificador de cliente de MMT
     *
     * @var integer
     */
    protected $client_id = 3;

    /**
     * Objeto clase que gestiona conexión y datos a gestionar.
     *
     * @var object
     */
    protected $clsMisc;
    /** 
     * URL del WS a enviar. 
     * lo ponemos aquí por ser más fácil
     *
     * @var string
     */
    protected $url = "https://apienterprise.multiopcion.es/api/lead/";

    protected $urlToken = "https://lieutenantworf.multiopcion.es/connect/token";

    /**
     * Credenciales. User
     *
     * @var string
     */
    protected $credentialsUser = "eretail";
    /**
     * Credenciales. Password
     *
     * @var string
     */
    protected $credentialsPassword = "3j'MKR}Z~QVd&JQF8pr@bJG5(ECD&3N(";

    /**
     * Credenciales. Id de Cliente
     *
     * @var string
     */
    protected $credentialsClientId = "enterprise_api_client";
    /**
     * Credenciales. Client Secret Password
     *
     * @var string
     */
    protected $credentialsClientSecret = "khBpqtxm2IbpEYCJwNR9CRdnsYEqF4deIBK2SUG5P5wIeFR2BB";

    /**
     * Token de conexión para mandar datos
     *
     * @var string
     */
    private $token = '';

    /**
     * Array a enviar.
     *
     * @var array
     */
    protected $arrData = [
        [
            'description' => 'LandingE-Retail',
            'leadType' => '1',
            'productType' => '1',
            'channelType' => '3',
            'Adn' => array(
                'provider' => 'ERETAILCOR',
                'media' => 'CPL',
                'campaign' => '0005',
                'plan' => '1509',
                'mode' => 'C2C',
                'offerTest' => 'INICIAL'
            ),
            'Client' => array(
                'name' => 'Metlife'
            ),
            'Origin' => array(
                'type' => '1',
                'url' => 'https://plan-dental.tuofertadeseguro.com/plan-dental-plus',
                'name' => 'MetlifeLanding'
            ),
            'Data' => array(
                'firstName' => '',
                'lastName' => '',
                'gender' => '', //2 HOMBRE, 1 MUJER
                'email' => null,
                'phoneNumber' => '',
                'mobileNumber' => '',
                'dni' => '',
                'birthDate' => '',
                'address' => null,
                'location' => '',
                'postalCode' => '',
                'smoker' => false,
                'fringe' => 0,
                'startCallDate' => ''


            )
        ]
    ];

    /**
     * Dependiendo del Environment en el que estamos, verificaremos en la conexión el certificado o no.
     *
     * @var boolean
     */
    protected $verifySslConnection = false;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->verifySslConnection = (env('APP_ENV') != 'local');
        $this->clsMisc = new AppMiscClass();
    }

    /**
     * Función privada que conecta con el sistema para pedir un token y empezar a enviar datos
     *
     * @return string uuid
     */
    private function getToken()
    {
        $arrData = array(
            'client_id' => $this->credentialsClientId,
            'client_secret' => $this->credentialsClientSecret,
            'grant_type' => 'password',
            'username' => $this->credentialsUser,
            'password' => $this->credentialsPassword
        );

        $this->token = '';



        $header = ["Content-Type" => "application/x-www-form-urlencoded", "cache-control" => "no-cache"];
        $client = new \GuzzleHttp\Client(['verify' => $this->verifySslConnection]);
        $response = $client->request('POST', $this->urlToken, ['form_params' => $arrData, 'headers' => $header]);
        $resp = (string) $response->getBody();

        //$this->info($response);
        // $this->info($resp);

        $statusCode = $response->getStatusCode();
        //unset($client);
        if (!$this->verifySslConnection)
            $this->info('Resp Token: ' . $statusCode);

        if ($statusCode == 200) {
            $respTokenArr = json_decode($resp, true);
            $this->token = $respTokenArr['access_token'];
            //dd($this->token);
            return true;
        } else {
            //dd('no hay por donde agarrarlo');
            return false;
        }
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Process to be started');
        //$now = Carbon::now()->format('Y-m-d\TH:i:s').'.010';


        $data = $this->clsMisc->ws_getRegisters($this->client_id);
$this->info('Items to be sent: '.count($data)."\n");
        if (count($data) > 0) {
            //Lo primero que tenemos que hacer es conseguir el token de conexión
            $respToken = $this->getToken();
            // dd($respToken);
            $bar = $this->output->createProgressBar(count($data));
            if ($respToken) {
                foreach ($data as $datum) {
                    $id = $datum->id;

                    $phone = trim($datum->phone);
                    $ip = trim($datum->ip);
                    $residence = strtoupper(trim($datum->add_info_1));
                    if (!$this->verifySslConnection)
                        $this->info($residence);
                    //dd($header);
                    if ($residence != 'SI') {
                        // dd('ha dicho que no');
                        $idLocal = $this->clsMisc->ws_setStartSent($id, 'POST', $this->url, 'Descartado por NO residente', '', '', true);
                        $this->clsMisc->ws_setEndSent($idLocal, $id, 0, 0, 'Descartado por NO residente', '');

                        $bar->advance();
                        continue;
                    }
                    $usrData = $this->arrData;
                    $usrData[0]['Data']['firstName'] = ucwords(strtolower(trim($datum->name)));
                    $usrData[0]['Data']['lastName'] = ucwords(strtolower(trim($datum->surname)));
                    $usrData[0]['Data']['phoneNumber'] = $phone;
                    $usrData[0]['Data']['mobileNumber'] = $phone;

                    $usrData[0]['Data']['startCallDate'] = Carbon::now()->format('Y-m-d\TH:i:s') . '.010';


                    //dd($usrData);
                    /**
                     * Insertamos registro en el log antes de mandar
                     */
                    $header = [
                        'Authorization' => 'Bearer ' . $this->token,
                        'Content-Type' => 'application/json',
                        'Content-Length' => strlen(json_encode($usrData))
                    ];


                    //                 dd($usrData);

                    //$usrData = json_decode('[{"description":"CoregistrosE-Retail","leadType":"1","productType":"1","channelType":"3","Adn":{"provider":"ERETAIL","media":"CPL","campaign":"0005","plan":"1509","mode":"C2C","offerTest":"INICIAL"},"Client":{"name":"Metlife"},"Origin":{"type":"1","url":"https:\/\/viajes.catalogodepremios.com","name":"SORTEO BELLEZA"},"Data":{"firstName":"Rosa","lastName":"Giner Pascual","gender":"1","email":"rogipas@hotmail.com","phoneNumber":"637557872","mobileNumber":"637557872","dni":"","birthDate":"1965-02-20T01:31:06.010","address":null,"location":"Valencia","postalCode":"46185","smoker":false,"fringe":0,"startCallDate":"2020-01-28T16:20:06.010"}}]',true);
                    //               dd($usrData);
                    $idLocal = $this->clsMisc->ws_setStartSent($id, 'POST', $this->url, json_encode($usrData), json_encode($header), '', true);
                    $error = false;
                    $client = new \GuzzleHttp\Client(['verify' => $this->verifySslConnection]);
                    try {
                        //$this->info(json_encode($header));
                        //dd(json_encode($usrData));
                        $response = $client->request('POST', $this->url, ['json' => $usrData, 'headers' => $header]);
                    } catch (\GuzzleHttp\Exception\ClientException $e) {

                        $response = $e->getResponse();
                        $error = true;
                        $resp = $response->getBody()->getContents();
                        $statusCode = (int) $response->getStatusCode();
                        $this->warn($statusCode);
                        $this->warn($resp);
                        // dd('Salimos con error');
                        //dd($response);

                    }
                    $statusCode = (int) $response->getStatusCode();
                    $resp = (string) $response->getBody();
                    if (!$this->verifySslConnection)
                    $this->info($statusCode);
                    //dd($resp);
                    $headerResp = [];
                    foreach ($response->getHeaders() as $name => $values) {
                        $headerResp[] = $name . ": " . implode(", ", $values);
                    }
                    if (!$this->verifySslConnection)
                        $this->info("Respuesta: " . $resp . "\n");
                    $resp = trim($resp);

                    $respArr = json_decode($resp);
                    $responseLeadArr = [];
                    if (json_last_error() == JSON_ERROR_NONE) {
                        $responseLeadArr = json_decode($resp, true);
                    }

                    $b_status = 0;
                    if ($statusCode == 201 && (isset($responseLeadArr[0]['isExcluded']) && boolval($responseLeadArr[0]['isExcluded']))) {
                        $b_status = 0;
                    } elseif ($statusCode == 200 || $statusCode == 201) {
                        $b_status = 1;
                    }
                    $this->clsMisc->ws_setEndSent($idLocal, $id, $b_status, $statusCode, $resp, $header);
                    unset($client);
                    $bar->advance();
                } //foreach
            }

            $this->warn('Terminamos el proceso' . "\n");
            $bar->finish();
            $this->info("\n");
            $this->info("\n");
            $this->info("\n");
            $this->info("\n");
        }
    }
}
