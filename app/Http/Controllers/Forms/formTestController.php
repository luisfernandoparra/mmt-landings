<?php

namespace App\Http\Controllers\Forms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Validator;
use Carbon\Carbon;

class formTestController extends Controller
{
    /**
     * Función privada que devuelve las provincias
     *
     * @return void
     */
    private function _get_Provinces()
    {
        return DB::table('provinces')->select(['id', 'province'])->where('id', '>', 0)->get();
    }


    /**
     * Función índice
     *
     * @param Request $request
     * @return void
     */
    function index(Request $request)
    {
        //$request->session()->flush();
        session(['emailValid' => false]);

        //Sacamos primero las provincias
        $source = (!empty($request->fuente) && is_numeric($request->fuente)) ? $request->fuente : 0;
        //dd($source);
        return view('welcome_test', ['provinces' => $this->_get_Provinces(), 'source' => $source]);
    }

    /**
     * Función llamada por Ajax. Chequea si un email es válido 
     *
     * @param Request $request
     * @return void
     */
    function checkEmail(Request $request)
    {
        session(['emailValid' => (bool)random_int(0, 1)]);
        return response()->json(['validEmail' => false, 'errors' => ['email' => 'Record is successfully added']]);
    }

    /**
     * Función que inserta los datos o devuelve error
     *
     * @param Request $request
     * @return void
     */
    function insertRegister(Request $request)
    {
        $rules = [
            'name' => 'required|string|max:255',
            'surname' => 'required|string|max:255',

            'email' => 'required|email|max:255',
            'phone' => 'required|regex:/^[67]{1}[0-9]{8}/|digits:9',
            'id_province' => 'required|numeric|exists:provinces,id',
            'zip' => 'required|digits_between:4,5'
        ];



        $validator = Validator::make($request->all(), $rules);
        $validator->after(function ($validator) {
            if (!session('emailValid'))
                $validator->errors()->add('email', 'Your email is still wrong!!!');
        });
        $validator->validate();
        // $validate = $request->validate($rules);
        /**
         * Si llegamos aquí, podemos recoger los datos
         */
        $name = $request->get('name', 'NombreVacío');
        $surname = $request->get('surname', 'ApellidoVacío');
        $email = $request->get('email', 'fake@fake.com');
        $id_province = $request->get('id_province', '0');
        $zip = $request->get('zip', '0');
        $phone = $request->get('phone', '0');
        $id = DB::table('registers')->insertGetId([
            'id_site' => $request->get('id_site', '0'),
            'id_source' => $request->get('id_source', '0'),
            'name' => $name,
            'surname' => $surname,
            'email' => $email,
            'id_province' => $id_province,
            'zip' => $zip,
            'phone' => $phone,
            'ip' => $request->ip(),
            'user_agent' => $request->header('User-Agent'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'b_accept_cookies' => session('emailValid', true),
            'date_accept_cookies' => Carbon::now()->format('Y-m-d H:i:s'),
            'b_accept_legal_issues' => !session('emailValid', true),
            'date_accept_legal_issues' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        session(['id'=>$id,'source'=>$request->get('id_source', '0'),'id_site'=>$request->get('id_site', '0')]);
        return view('welcome', ['id' => $id, 'message' => 'Record is successfully added', 'success' => session('emailValid')]);
    }
}
