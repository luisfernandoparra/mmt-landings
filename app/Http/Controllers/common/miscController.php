<?php
/*
* DEPRECATED
* Pasarlo a Clase
 * Created on Thu May 16 2019
 *
 * Copyright (c) 2019 e-retailData Technology SL
 */

/**
 * Controlador que contendrá funciones comunes como por ejemplo la validación de email o teléfono
 */

namespace App\Http\Controllers\common;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Validator;
use Carbon\Carbon;
use GuzzleHttp\Client;
use function GuzzleHttp\json_decode;

class miscController extends Controller
{

    /**
     * Función que chequeará la validación de un email. Llamará a la api que tenemos para validar el teléfono
     *
     * @param Request $request 
     * @return boolean
     */
    function checkEmail(Request $request)
    {
        $clientName = $request->get('client', 'mmt');
        $email = $request->get('email', 'fakemail@email.com');

        $usr = config('misc.emailChecker.clients.' . $clientName . '.usr');
        $pwd = config('misc.emailChecker.clients.' . $clientName . '.pwd');
        $url = config('misc.emailChecker.url');
        $url = sprintf($url, $usr, $pwd, $email);
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $url);

        $statusCode = $response->getStatusCode(); # 200
        $statusContent = $response->getHeaderLine('content-type'); # 'application/json; charset=utf8'
        $resp = $response->getBody();

        $respJson = json_decode($resp, true);

        $exists = $respJson['success'];
        $statusId = $respJson['statusId'];
        $response = ['validEmail' => (int)$exists];
        if (!$exists) {
            $response['errors']=['El email no es correcto o no existe'];
        }
        session(['emailValid' => $exists]);
        return response()->json($response);
    }
}
