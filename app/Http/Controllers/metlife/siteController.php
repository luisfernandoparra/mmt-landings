<?php

namespace App\Http\Controllers\metlife;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Validator;
use Carbon\Carbon;
use App\miscClass\miscClass as AppMiscClass;
use GuzzleHttp\Client;

class siteController extends Controller
{
    private $email = '';
    private $id_site = 0;
    private $phone = '';
    private $validEmail;


    /**
     * **************************************************
     * Variables de envío de datos a Metlife vía WS
     * **************************************************
     */


    /**
     * Constante que define el identificador de cliente de MMT
     *
     * @var integer
     */
    protected $client_id = 3;

    /**
     * Objeto clase que gestiona conexión y datos a gestionar.
     *
     * @var object
     */
    protected $clsMisc;
/** 
     * URL del WS a enviar. 
     * lo ponemos aquí por ser más fácil
     *
     * @var string
     */
    protected $url = "https://apienterprise.multiopcion.es/api/lead/";

    protected $urlToken = "https://lieutenantworf.multiopcion.es/connect/token";

    /**
     * Credenciales. User
     *
     * @var string
     */
    protected $credentialsUser = "eretail";
    /**
     * Credenciales. Password
     *
     * @var string
     */
    protected $credentialsPassword = "3j'MKR}Z~QVd&JQF8pr@bJG5(ECD&3N(";

    /**
     * Credenciales. Id de Cliente
     *
     * @var string
     */
    protected $credentialsClientId = "enterprise_api_client";
    /**
     * Credenciales. Client Secret Password
     *
     * @var string
     */
    protected $credentialsClientSecret = "khBpqtxm2IbpEYCJwNR9CRdnsYEqF4deIBK2SUG5P5wIeFR2BB";


    /**
     * Token de conexión para mandar datos
     *
     * @var string
     */
    private $token = '';

    /**
     * Array a enviar.
     * @date 2020.05.20
     * 
     * Cambiamos a petición de Metlife ciertos parámetros de envío de datos
     * 
     * plan: 1509 => 1527
     * 
     * 
     *
     * @var array
     */
    protected $arrData = [
        [
            'description' => 'LandingE-Retail',
            'leadType' => '1',
            'productType' => '1',
            'channelType' => '3',
            'Adn' => array(
                'provider' => 'ERETAILCOR',
                'media' => 'CPL',
                'campaign' => '0005',
                'plan' => '1527',
                'mode' => 'C2C',
                'offerTest' => 'INICIAL'
            ),
            'Client' => array(
                'name' => 'Metlife'
            ),
            'Origin' => array(
                'type' => '1',
                'url' => 'https://plan-dental.tuofertadeseguro.com/plan-dental-plus',
                'name' => 'MetlifeLanding'
            ),
            'Data' => array(
                'firstName' => '',
                'lastName' => '',
                'gender' => '', //2 HOMBRE, 1 MUJER
                'email' => null,
                'phoneNumber' => '',
                'mobileNumber' => '',
                'dni' => '',
                'birthDate' => '',
                'address' => null,
                'location' => '',
                'postalCode' => '',
                'smoker' => false,
                'fringe' => 0,
                'startCallDate' => ''


            )
        ]
    ];

    /**
     * Dependiendo del Environment en el que estamos, verificaremos en la conexión el certificado o no.
     *
     * @var boolean
     */
    protected $verifySslConnection = false;

    /**
     * Entorno donde estamos. local o production
     *
     * @var string
     */
    protected $environment = 'production';



    function testThanks(Request $request)
    {

        $env = env('APP_ENV');
            
            $this->environment = (!empty($env)) ? $env : $this->environment;
            $cadConfig = 'misc.metlife.'.$this->environment.'.url_data';
            $url = config($cadConfig);
            $val = config('misc.metlife.'.$this->environment.'.test');

        return view('metlife.campaign_1.thanksTest', ['result' => 1,'environment'=>$this->environment, 'id' => 12, 'url'=>$url,'cadconfig'=>$cadConfig,'valtest'=>$val]);
    }

    /**
     * Función índice de la campaña id 1
     *
     * @param Request $request
     * @return void
     */
    function indexCampaign_1(Request $request)
    {
        //$request->session()->flush();
        session([
            'emailValid' => false,
            'clientName' => 'mmt',
            'cookieAccept' => false,
            'id_client' => 3,
            'id_site' => 2
        ]);

        $source = (!empty($request->fuente) && is_numeric($request->fuente)) ? $request->fuente : 0;
        $source = (!empty($source)) ? $source : ((!empty($request->source) && is_numeric($request->source)) ? $request->source : 0);


        $policiesCookies = DB::table('legal_epigraphs')->join('legal_epigraphs_sites', function ($join) {
            $join->on('legal_epigraphs.id', '=', 'legal_epigraphs_sites.id_legal_epigraph')
                ->where([['legal_epigraphs_sites.id_site', 2], ['legal_epigraphs_sites.b_active', 1]]);
        })->where('id_legal_epigraphs_types', 2)->select(['legal_epigraphs.id', 'legal_epigraphs.title', 'legal_epigraphs.epigraph', 'legal_epigraphs_sites.order'])->orderBy('legal_epigraphs_sites.order', 'asc')->get();


        return view('metlife.campaign_1.home', ['source' => $source, 'validEmail' => 0, 'cookiesPolicy' => $policiesCookies]);
    }









 /**
     * Función privada que conecta con el sistema para pedir un token y empezar a enviar datos
     *
     * @return string uuid
     */
    private function getToken()
    {
        $arrData = array(
            'client_id' => ($this->environment=='local') ? config('misc.metlife.'.$this->environment.'.credentialsClientId') : $this->credentialsClientId,
            'client_secret' => ($this->environment=='local') ? config('misc.metlife.'.$this->environment.'.credentialsClientSecret') : $this->credentialsClientSecret,
            'grant_type' => 'password',
            'username' => ($this->environment=='local') ? config('misc.metlife.'.$this->environment.'.credentialsUser') : $this->credentialsUser,
            'password' => ($this->environment=='local') ? config('misc.metlife.'.$this->environment.'.credentialsPassword') : $this->credentialsPassword
        );

        //dump($this->environment);
        //dd($arrData);
        $this->token = '';

       
        $url = ($this->environment=='local') ? config('misc.metlife.'.$this->environment.'.url_token') : $this->urlToken;

        $header = ["Content-Type" => "application/x-www-form-urlencoded", "cache-control" => "no-cache"];
        $client = new \GuzzleHttp\Client(['verify' => $this->verifySslConnection]);
        $response = $client->request('POST', $url, ['form_params' => $arrData, 'headers' => $header]);
        $resp = (string) $response->getBody();

        //$this->info($response);
        // $this->info($resp);

        $statusCode = $response->getStatusCode();
      

        if ($statusCode == 200) {
            $respTokenArr = json_decode($resp, true);
            $this->token = $respTokenArr['access_token'];
            //dd($this->token);
            return true;
        } else {
            //dd('no hay por donde agarrarlo');
            return false;
        }
    }




    /**
     * Función que para la campaña 1 inserta los datos de registro en la bbdd
     *
     * @param Request $request Datos recogidos
     * @return void
     */
    function insertRegisterCampaign_1(Request $request)
    {
        $acceptCookies = !empty($_COOKIE['acceptCookies']) && (bool) $_COOKIE['acceptCookies'];
        $acceptCookiesDateTime = $acceptCookies && !empty($_COOKIE['cookieSetDate']) ? trim($_COOKIE['cookieSetDate']) : null;

        $rules = [
            'name' => 'required|string|max:255',
            'surname' => 'required|string|max:255',

            'phone' => 'required|regex:/^[67]{1}[0-9]{8}/',
            'residence' => 'required|in:SI,NO',
            'legalpolicy' => 'accepted'
        ];

        $id_site = (!empty($request->id_site) && is_numeric($request->id_site)) ? $request->id_site : $request->session()->get('id_site', 0);
        $phone = (!empty($request->phone) && is_numeric($request->phone)) ? $request->phone : 0;

        //$clsMisc = new AppMiscClass();
        //$this->validEmail = $clsMisc->emailCheck($email, 'mmt');




        $this->id_site = $id_site;

        $this->phone = $phone;
        $validator = Validator::make($request->all(), $rules);
        $validator->after(function ($validator) {

            if (!empty($this->id_site) && !empty($this->phone)) {
                $cont = DB::table('registers')->where([['id_site', $this->id_site], ['phone', $this->phone]])->count();
                if ($cont > 0) {
                    $validator->errors()->add('email', 'El teléfono YA existe en el sistema. Por favor, selecciona otro');
                }
            }
        });
        $validator->validate();

        //Insertamos
        $name = $request->get('name', 'NombreVacío');
        $surname = $request->get('surname', 'ApellVacío');
        $phone = $request->get('phone', '0');
        $legalpolicy = $request->get('legalpolicy', 1);
        $residence = $request->get('residence', 'SI');
        $legalpolicy = ($legalpolicy == 'on' || $legalpolicy == '1') ? 1 : $legalpolicy;

        $id = DB::table('registers')->insertGetId([
            'id_site' => $request->get('id_site', '0'),
            'id_source' => $request->get('id_source', '0'),
            'name' => $name,
            'surname' => $surname,
            'phone' => $phone,
            'add_info_1' => $residence,
            'ip' => $request->ip(),
            'user_agent' => $request->header('User-Agent'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'b_accept_cookies' => $acceptCookies,
            'date_accept_cookies' => $acceptCookiesDateTime,
            'b_accept_legal_issues' => $legalpolicy,
            'date_accept_legal_issues' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        //POr la RGPD guardamos exactamente los literales de aceptación de cookies y políticas de Proteccón de Datos
        $policies = DB::table('legal_epigraphs')->join('legal_epigraphs_sites', function ($join) {
            $join->on('legal_epigraphs.id', '=', 'legal_epigraphs_sites.id_legal_epigraph')
                ->where([['legal_epigraphs_sites.id_site', $this->id_site], ['legal_epigraphs_sites.b_active', 1]]);
        })->where('id_legal_epigraphs_types', 1)->select(['legal_epigraphs.id', 'legal_epigraphs.title', 'legal_epigraphs.epigraph'])->get();
        foreach ($policies as $policy) {
            DB::table('registers_rgpd')->insert([
                'registers_id' => $id,
                'legal_epigraphs_id' => $policy->id,
                'epigraph' => $policy->epigraph,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
        }

        // if ($acceptCookies) {
        $policiesCookies = DB::table('legal_epigraphs')->join('legal_epigraphs_sites', function ($join) {
            $join->on('legal_epigraphs.id', '=', 'legal_epigraphs_sites.id_legal_epigraph')
                ->where([['legal_epigraphs_sites.id_site', $this->id_site], ['legal_epigraphs_sites.b_active', 1]]);
        })->where('id_legal_epigraphs_types', 2)->select(['legal_epigraphs.id', 'legal_epigraphs.title', 'legal_epigraphs.epigraph'])->get();
        foreach ($policiesCookies as $policy) {
            DB::table('registers_rgpd')->insert([
                'registers_id' => $id,
                'legal_epigraphs_id' => $policy->id,
                'epigraph' => $policy->epigraph,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
        }
        $result = (int) (strtoupper(trim($residence)) == 'SI');


        if ($result) {
            /**
             * Si es que SI, lo intentamos mandar al cliente en tiempo real. 
             */
            $env = env('APP_ENV');
            
            $this->environment = (!empty($env)) ? $env : $this->environment;
            //dd($this->environment);
            $this->verifySslConnection = ($this->environment=='local') ? config('misc.metlife.'.$this->environment.'.ssl_verify_token') : true;
            $this->clsMisc = new AppMiscClass();
            $respToken = $this->getToken();
           
            if ($respToken) {
                $usrData = $this->arrData;
                    $usrData[0]['Data']['firstName'] = ucwords(strtolower(trim($name)));
                    $usrData[0]['Data']['lastName'] = ucwords(strtolower(trim($surname)));
                    $usrData[0]['Data']['phoneNumber'] = $phone;
                    $usrData[0]['Data']['mobileNumber'] = $phone;

                    $usrData[0]['Data']['startCallDate'] = Carbon::now()->format('Y-m-d\TH:i:s') . '.010';

                     /**
                     * Insertamos registro en el log antes de mandar
                     */
                    /**
                     * Cabecera para mandar los datos
                     */
                    $header = [
                        'Authorization' => 'Bearer ' . $this->token,
                        'Content-Type' => 'application/json',
                        'Content-Length' => strlen(json_encode($usrData))
                    ];
                    $url = ($this->environment=='local') ? config('misc.metlife.'.$this->environment.'.url_data') : $this->url;
                    $idLocal = $this->clsMisc->ws_setStartSent($id, 'POST', $url, json_encode($usrData), json_encode($header), '', true);
                    $error = false;
                    $client = new \GuzzleHttp\Client(['verify' => $this->verifySslConnection]);
                    try {
                        //$this->info(json_encode($header));
                        //dd(json_encode($usrData));
                        $response = $client->request('POST', $url, ['json' => $usrData, 'headers' => $header]);
                    } catch (\GuzzleHttp\Exception\ClientException $e) {

                        $response = $e->getResponse();
                        $error = true;
                        $resp = $response->getBody()->getContents();
                        $statusCode = (int) $response->getStatusCode();
                        
                        // dd('Salimos con error');
                        //dd($response);

                    }
                    $statusCode = (int) $response->getStatusCode();
                    $resp = (string) $response->getBody();
                    //dump(config('misc.metlife.'.$this->environment.'.url_data'));
                    //dd($resp);
                    $headerResp = [];
                    foreach ($response->getHeaders() as $name => $values) {
                        $headerResp[] = $name . ": " . implode(", ", $values);
                    }
                    $resp = trim($resp);

                    $respArr = json_decode($resp);
                    $responseLeadArr = [];
                    if (json_last_error() == JSON_ERROR_NONE) {
                        $responseLeadArr = json_decode($resp, true);

                    }

                    $result = 0;
                    if ($statusCode == 201 && (isset($responseLeadArr[0]['isExcluded']) && boolval($responseLeadArr[0]['isExcluded']))) {
                        $result = 2;
                    } elseif ($statusCode == 200 || $statusCode == 201) {
                        $result = 1;
                    }
                    $this->clsMisc->ws_setEndSent($idLocal, $id, $result, $statusCode, $resp, $header);
                    unset($client);
                    

            } //if respToken

            unset($this->clsMisc);

        }

        return view('metlife.campaign_1.thanks', ['result' => $result, 'id' => $id, 'acceptCookies' => $acceptCookies]);
    }
}
