<?php

namespace App\Http\Controllers\keycoes;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Validator;
use Carbon\Carbon;
use App\miscClass;
use App\miscClass\miscClass as AppMiscClass;


class siteController extends Controller
{
    private $email = '';
    private $id_site = 0;
    private $phone = '';
    private $validEmail;



    /**
     * Constante que define el identificador de cliente de Keycoes
     *
     * @var integer
     */
    protected $client_id = 4;

    /**
     * Objeto clase que gestiona conexión y datos a gestionar.
     *
     * @var object
     */
    protected $clsMisc;
    /**
     * Función landing home de la campaña KeyCoes
     *
     * @param Request $request
     * @return void
     */
    function indexCampaign_1(Request $request)
    {
        session([
            'emailValid' => false,
            'clientName' => 'keycoes',
            'cookieAccept' => false,
            'id_client' => 4,
            'id_site' => 3
        ]);
        $source = (!empty($request->fuente) && is_numeric($request->fuente)) ? $request->fuente : 0;
        $source = (!empty($source)) ? $source : ((!empty($request->source) && is_numeric($request->source)) ? $request->source : 0);

        $policiesCookies = DB::table('legal_epigraphs')->join('legal_epigraphs_sites', function ($join) {
            $join->on('legal_epigraphs.id', '=', 'legal_epigraphs_sites.id_legal_epigraph')
                ->where([['legal_epigraphs_sites.id_site', 3], ['legal_epigraphs_sites.b_active', 1]]);
        })->where('id_legal_epigraphs_types', 2)->select(['legal_epigraphs.id', 'legal_epigraphs.title', 'legal_epigraphs.epigraph', 'legal_epigraphs_sites.order'])->orderBy('legal_epigraphs_sites.order', 'asc')->get();

        $policies = DB::table('legal_epigraphs')->join('legal_epigraphs_sites', function ($join) {
            $join->on('legal_epigraphs.id', '=', 'legal_epigraphs_sites.id_legal_epigraph')
                ->where([['legal_epigraphs_sites.id_site', 3], ['legal_epigraphs_sites.b_active', 1]]);
        })->where('id_legal_epigraphs_types', 1)->select(['legal_epigraphs.id', 'legal_epigraphs.title', 'legal_epigraphs.epigraph', 'legal_epigraphs_sites.order'])->orderBy('legal_epigraphs_sites.order', 'asc')->get();


        return view('keycoes.home_imagenes_new', ['source' => $source, 'validEmail' => 0, 'cookiesPolicy' => $policiesCookies, 'privatePolicies' => $policies]);
    }


    /**
     * Función auxiliar que manda email de recibimiento de datos al cliente
     *
     * @param array $data Array con los datos a enviar
     * @return void
     */
    private function sendEmailToClient($data)
    {
        $sendto = 'luisfer.parra@gmail.com';
        $sendto = 'coke.ruiz@keycoes.es';
        $url = "https://ws-server.e-retaildata.com/webservices/transactionalEmails/index.php";




        $htmlEmail = '<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
        <title>Keycoes new Register</title>
        <style>
    table {
      font-family: arial, sans-serif;
      border-collapse: collapse;
      width: 100%;
    }
    
    td, th {
      border: 1px solid #dddddd;
      text-align: left;
      padding: 8px;
    }
    
    tr:nth-child(even) {
      background-color: #dddddd;
    }

    th {
        background-color: #f2f2f2;
    }
    </style>
        </head>
        <html><body>
        <hr>
<h3>New Register</h3>
<br>
<table>
    <thead>
        <tr>
            <th>Elemento</th><th>Valor</th>
        </tr>
    </thead>
    <tbody>
    {{ body }}
    </tbody>
</table><br><br><hr>

        </body></html>
        ';
        //primero eliminamos campos que no nos valen
        unset($data['id_site']);
        unset($data['id_source']);
        unset($data['date_accept_cookies']);
        unset($data['b_sent_client']);
        unset($data['date_accept_legal_issues']);

        $bodyTable = "";
        foreach ($data as $k => $v) {
            $val = $v;
            $th = $k;
            switch ($k) {
                case 'name':
                    $th = "Nombre";
                    break;
                case 'add_info_1':
                    $th = 'Compañía';
                    break;
                case 'b_accept_legal_issues':
                    $th = "Acepta Política Privacidad";
                    $val = ($val == '1') ? 'Si' : 'No';

                    break;
                case 'b_accept_cookies':
                    $th = "Acepta Cookies";
                    $val = ($val == '1') ? 'Si' : 'No';
                    break;
                case 'email':
                case 'ip':
                    $th = ucwords(trim($k));
                    break;
                case 'created_at':
                    $th = 'Fecha Registro';
                    break;
                default:
                    continue;
                    break;
            }
            $bodyTable .= "<tr>";

            $bodyTable .= "<td>$th</td><td>$val</td>";
            $bodyTable .= "</tr>";
        }

        $html = str_replace('{{ body }}', $bodyTable, $htmlEmail);

        $arr = [
            'user' => 'SES-WSSEVER2', 'data' =>
            [
                [
                    'receiver' => $sendto,
                    'subject' => 'KeyCoes New Register ' . Carbon::now()->format('Y-m-d H:i:s'),
                    'html' => $html,
                    'text' => 'Text de envío'
                ]
            ]
        ];
        $client = new \GuzzleHttp\Client();
        $response = $client->post($url, ['json' => $arr, 'http_errors' => false]);
    }

    /**
     * Función que inserta el dato dentro de la tabla correspondiente y hace lo que sea  necesario
     *
     * @param Request $request
     * @return void
     */
    function insertRegisterCampaign_1(Request $request)
    {

        $acceptCookies = !empty($_COOKIE['acceptCookies']) && (bool) $_COOKIE['acceptCookies'];
        $acceptCookiesDateTime = $acceptCookies && !empty($_COOKIE['cookieSetDate']) ? trim($_COOKIE['cookieSetDate']) : null;

        $rules = [
            'name' => 'required|string|max:255',
            'company' => 'required|string|max:255',
            'email' => 'required|email|max:255',
            'legalpolicy' => 'accepted'
        ];

        $email = (!empty($request->email)) ? strtolower($request->email) : '';

        $clsMisc = new AppMiscClass();

        $this->validEmail = $clsMisc->emailCheck($email, 'mmt');


        $id_site = (!empty($request->id_site) && is_numeric($request->id_site)) ? $request->id_site : $request->session()->get('id_site', 0);
        $this->id_site = $id_site;
        $this->email = $email;

        $validator = Validator::make($request->all(), $rules);
        $validator->after(function ($validator) {
            if (/*!session('emailValid') || */$this->validEmail == 0) {
                $validator->errors()->add('email', 'El email no es correcto o no existe');
            } else {
                if (!empty($this->id_site)) {
                    $cont = DB::table('registers')->where([['id_site', $this->id_site], ['email', $this->email]])->count();
                    if ($cont > 0) {
                        $validator->errors()->add('email', 'El email YA existe en el sistema. Por favor, selecciona otro');
                    }
                }
            }
            /*
            if (!empty($this->id_site) && !empty($this->phone)) {
                $cont = DB::table('registers')->where([['id_site', $this->id_site], ['phone', $this->phone]])->count();
                if ($cont > 0) {
                    $validator->errors()->add('email', 'El teléfono YA existe en el sistema. Por favor, selecciona otro');
                }
            }
            */
        });
        $validator->validate();


        //Si llegamos aquí podemos insertar el registro
        $name = $request->get('name', 'NombreVacío');

        $email = $request->get('email', 'fake@fake.com');
        $company = $request->get('company', 'FakeConmpany LTD');

        $legalpolicy = $request->get('legalpolicy', 1);
        $legalpolicy = $legalpolicy == 'on' ? 1 : $legalpolicy;
        $insertArr = [
            'id_site' => $request->get('id_site', '0'),
            'id_source' => $request->get('id_source', '0'),
            'name' => $name,
            'email' => $email,
            'add_info_1' => $company,
            'ip' => $request->ip(),
            'user_agent' => $request->header('User-Agent'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'b_accept_cookies' => $acceptCookies,
            'date_accept_cookies' => $acceptCookiesDateTime,
            'b_accept_legal_issues' => $legalpolicy,
            'date_accept_legal_issues' => Carbon::now()->format('Y-m-d H:i:s'),
            'b_sent_client' => 1
        ];
       // $this->sendEmailToClient($insertArr);
        $id = DB::table('registers')->insertGetId($insertArr);

        //POr la RGPD guardamos exactamente los literales de aceptación de cookies y políticas de Proteccón de Datos
        $policies = DB::table('legal_epigraphs')->join('legal_epigraphs_sites', function ($join) {
            $join->on('legal_epigraphs.id', '=', 'legal_epigraphs_sites.id_legal_epigraph')
                ->where([['legal_epigraphs_sites.id_site', $this->id_site], ['legal_epigraphs_sites.b_active', 1]]);
        })->where('id_legal_epigraphs_types', 1)->select(['legal_epigraphs.id', 'legal_epigraphs.title', 'legal_epigraphs.epigraph'])->get();
        foreach ($policies as $policy) {
            DB::table('registers_rgpd')->insert([
                'registers_id' => $id,
                'legal_epigraphs_id' => $policy->id,
                'epigraph' => $policy->epigraph,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
        }
        // if ($acceptCookies) {
        $policiesCookies = DB::table('legal_epigraphs')->join('legal_epigraphs_sites', function ($join) {
            $join->on('legal_epigraphs.id', '=', 'legal_epigraphs_sites.id_legal_epigraph')
                ->where([['legal_epigraphs_sites.id_site', $this->id_site], ['legal_epigraphs_sites.b_active', 1]]);
        })->where('id_legal_epigraphs_types', 2)->select(['legal_epigraphs.id', 'legal_epigraphs.title', 'legal_epigraphs.epigraph'])->get();
        foreach ($policiesCookies as $policy) {
            DB::table('registers_rgpd')->insert([
                'registers_id' => $id,
                'legal_epigraphs_id' => $policy->id,
                'epigraph' => $policy->epigraph,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
        }
        session(
            ['result' => 1, 'id' => $id, 'acceptCookies' => $acceptCookies, 'name' => $name, 'email' => $email, 'company' => $company, 'privatePolicies' => $policies, 'cookiesPolicy' => $policiesCookies]
        );

        // }
        //mandamos por correo o hacemos lo que tengamos que hacer
        //return view('keycoes.thanks_c1', ['result' => 1, 'id' => $id, 'acceptCookies' => $acceptCookies, 'name' => $name, 'email' => $email, 'company' => $company, 'privatePolicies' => $policies, 'cookiesPolicy' => $policiesCookies]);
        return redirect()->route('keycoes-thanks');
    }

    function gracias_1(Request $request)
    {
       
       
        $acceptCookies = !empty($_COOKIE['acceptCookies']) && (bool) $_COOKIE['acceptCookies'];
        $id = $request->session()->get('id',0);
        $name = $request->session()->get('name','fake');
        $email = $request->session()->get('email','fake@fake.com');
        $company =  $request->session()->get('company','FakeCompany SL');
        $id_site = (!empty($request->id_site) && is_numeric($request->id_site)) ? $request->id_site : $request->session()->get('id_site', 0);
        $this->id_site = $id_site;
        $policies = ($request->session()->has('privatePolicies')) ? $request->session('privatePolicies') : DB::table('legal_epigraphs')->join('legal_epigraphs_sites', function ($join) {
            $join->on('legal_epigraphs.id', '=', 'legal_epigraphs_sites.id_legal_epigraph')
                ->where([['legal_epigraphs_sites.id_site', $this->id_site], ['legal_epigraphs_sites.b_active', 1]]);
        })->where('id_legal_epigraphs_types', 1)->select(['legal_epigraphs.id', 'legal_epigraphs.title', 'legal_epigraphs.epigraph'])->get();
        $policiesCookies = ($request->session()->has('cookiesPolicy')) ? $request->session('cookiesPolicy') : DB::table('legal_epigraphs')->join('legal_epigraphs_sites', function ($join) {
            $join->on('legal_epigraphs.id', '=', 'legal_epigraphs_sites.id_legal_epigraph')
                ->where([['legal_epigraphs_sites.id_site', $this->id_site], ['legal_epigraphs_sites.b_active', 1]]);
        })->where('id_legal_epigraphs_types', 2)->select(['legal_epigraphs.id', 'legal_epigraphs.title', 'legal_epigraphs.epigraph'])->get();
        session([
            'emailValid' => false,
            'clientName' => 'keycoes',
            'cookieAccept' => false,
            'id_client' => 4,
            'id_site' => 3
        ]);
        return view('keycoes.thanks_c1', ['result' => 1, 'id' => $id, 'acceptCookies' => $acceptCookies, 'name' => $name, 'email' => $email, 'company' => $company, 'privatePolicies' => $policies, 'cookiesPolicy' => $policiesCookies]);
    }
}