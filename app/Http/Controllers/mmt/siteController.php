<?php

namespace App\Http\Controllers\mmt;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Validator;
use Carbon\Carbon;
use App\miscClass;
use App\miscClass\miscClass as AppMiscClass;

/**
 * Controlador de las campañas de MMT
 */

class siteController extends Controller
{

    private $email = '';
    private $id_site = 0;
    private $phone = '';
    private $validEmail;
    /**
     * Función índice de la campaña id 1
     *
     * @param Request $request
     * @return void
     */
    function indexCampaign_1(Request $request)
    {
        //$request->session()->flush();
        session([
            'emailValid' => false,
            'clientName' => 'mmt',
            'cookieAccept' => false,
            'id_client' => 1,
            'id_site' => 1
        ]);

        //Sacamos primero las provincias
        $source = (!empty($request->fuente) && is_numeric($request->fuente)) ? $request->fuente : 0;
        //dd($source);
        //Sacamos las políticas de Cookies y las posibles políticas de Privacidad

        $policiesCookies = DB::table('legal_epigraphs')->join('legal_epigraphs_sites', function ($join) {
            $join->on('legal_epigraphs.id', '=', 'legal_epigraphs_sites.id_legal_epigraph')
                ->where([['legal_epigraphs_sites.id_site', 1], ['legal_epigraphs_sites.b_active', 1]]);
        })->where('id_legal_epigraphs_types', 2)->select(['legal_epigraphs.id', 'legal_epigraphs.title', 'legal_epigraphs.epigraph', 'legal_epigraphs_sites.order'])->orderBy('legal_epigraphs_sites.order', 'asc')->get();

        $policies = DB::table('legal_epigraphs')->join('legal_epigraphs_sites', function ($join) {
            $join->on('legal_epigraphs.id', '=', 'legal_epigraphs_sites.id_legal_epigraph')
                ->where([['legal_epigraphs_sites.id_site', 1], ['legal_epigraphs_sites.b_active', 1]]);
        })->where('id_legal_epigraphs_types', 1)->select(['legal_epigraphs.id', 'legal_epigraphs.title', 'legal_epigraphs.epigraph', 'legal_epigraphs_sites.order'])->orderBy('legal_epigraphs_sites.order', 'asc')->get();


        return view('mmt.campaign_1.home', ['source' => $source, 'validEmail' => 0, 'cookiesPolicy' => $policiesCookies, 'policyMain' => $policies[0], 'policyAdd' => $policies[1]]);
    }


    /**
     * Función que para la campaña 1 inserta los datos de registro en la bbdd
     *
     * @param Request $request Datos recogidos
     * @return void
     */
    function insertRegisterCampaign_1(Request $request)
    {

        $acceptCookies = !empty($_COOKIE['acceptCookies']) && (bool)$_COOKIE['acceptCookies'];
        $acceptCookiesDateTime = $acceptCookies && !empty($_COOKIE['cookieSetDate']) ? trim($_COOKIE['cookieSetDate']) : null;
        $rules = [
            'name' => 'required|string|max:255',

            'email' => 'required|email|max:255',
            'phone' => 'required|regex:/^[67]{1}[0-9]{8}/',
            'hometype' => 'required|string|in:propiedad,alquiler',
            'enddatepolicy' => 'required|date|after_or_equal:today',
            'legalpolicy' => 'accepted'
        ];

        $id_site = (!empty($request->id_site) && is_numeric($request->id_site)) ? $request->id_site : $request->session()->get('id_site',0);
        $email = (!empty($request->email)) ? strtolower($request->email) : '';
        $phone = (!empty($request->phone) && is_numeric($request->phone)) ? $request->phone : 0;

        $clsMisc = new AppMiscClass();
        $this->validEmail = $clsMisc->emailCheck($email, 'mmt');




        $this->id_site = $id_site;
        $this->email = $email;
        $this->phone = $phone;
        $validator = Validator::make($request->all(), $rules);
        $validator->after(function ($validator) {
            if (/*!session('emailValid') || */$this->validEmail == 0) {
                $validator->errors()->add('email', 'El email no es correcto o no existe');
            } else {
                if (!empty($this->id_site)) {
                    $cont = DB::table('registers')->where([['id_site', $this->id_site], ['email', $this->email]])->count();
                    if ($cont > 0) {
                        $validator->errors()->add('email', 'El email YA existe en el sistema. Por favor, selecciona otro');
                    }
                }
            }
            if (!empty($this->id_site) && !empty($this->phone)) {
                $cont = DB::table('registers')->where([['id_site', $this->id_site], ['phone', $this->phone]])->count();
                if ($cont > 0) {
                    $validator->errors()->add('email', 'El teléfono YA existe en el sistema. Por favor, selecciona otro');
                }
            }
        });
        $validator->validate();

        //Si llegamos aquí podemos insertar el registro
        $name = $request->get('name', 'NombreVacío');

        $email = $request->get('email', 'fake@fake.com');
        $phone = $request->get('phone', '0');
        $homeType = $request->get('hometype', 'propiedad');
        $enddatepolicy = $request->get('enddatepolicy', '0000-00-00');
        $legalpolicy = $request->get('legalpolicy', 1);
        $legalpolicy = $legalpolicy == 'on' ? 1 : $legalpolicy;
        $id = DB::table('registers')->insertGetId([
            'id_site' => $request->get('id_site', '0'),
            'id_source' => $request->get('id_source', '0'),
            'name' => $name,
            'email' => $email,
            'phone' => $phone,
            'add_info_1' => $homeType,
            'add_info_2' => $enddatepolicy,
            'ip' => $request->ip(),
            'user_agent' => $request->header('User-Agent'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'b_accept_cookies' => $acceptCookies,
            'date_accept_cookies' => $acceptCookiesDateTime,
            'b_accept_legal_issues' => $legalpolicy,
            'date_accept_legal_issues' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //POr la RGPD guardamos exactamente los literales de aceptación de cookies y políticas de Proteccón de Datos
        $policies = DB::table('legal_epigraphs')->join('legal_epigraphs_sites', function ($join) {
            $join->on('legal_epigraphs.id', '=', 'legal_epigraphs_sites.id_legal_epigraph')
                ->where([['legal_epigraphs_sites.id_site', $this->id_site], ['legal_epigraphs_sites.b_active', 1]]);
        })->where('id_legal_epigraphs_types', 1)->select(['legal_epigraphs.id', 'legal_epigraphs.title', 'legal_epigraphs.epigraph'])->get();
        foreach ($policies as $policy) {
            DB::table('registers_rgpd')->insert([
                'registers_id'=>$id,
                'legal_epigraphs_id'=>$policy->id,
                'epigraph'=>$policy->epigraph,
                'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
            ]);
        }

       // if ($acceptCookies) {
            $policiesCookies = DB::table('legal_epigraphs')->join('legal_epigraphs_sites', function ($join) {
                $join->on('legal_epigraphs.id', '=', 'legal_epigraphs_sites.id_legal_epigraph')
                    ->where([['legal_epigraphs_sites.id_site', $this->id_site], ['legal_epigraphs_sites.b_active', 1]]);
            })->where('id_legal_epigraphs_types', 2)->select(['legal_epigraphs.id', 'legal_epigraphs.title', 'legal_epigraphs.epigraph'])->get();
            foreach ($policiesCookies as $policy) {
                DB::table('registers_rgpd')->insert([
                    'registers_id'=>$id,
                    'legal_epigraphs_id'=>$policy->id,
                    'epigraph'=>$policy->epigraph,
                    'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
                ]);
            }
    
       // }

        // session(['result'=>1,'id'=>$id,'source'=>$request->get('id_source', '0'),'id_site'=>$request->get('id_site', '0')]);
        //$request->session()->flush();
        return view('mmt.campaign_1.thanks', ['result' => 1, 'id' => $id,'acceptCookies'=>$acceptCookies]);
    }
}
