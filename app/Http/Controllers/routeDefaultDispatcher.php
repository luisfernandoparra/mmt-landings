<?php

/**
 * Enrutador dependiendo del dominio si se accede a / de un dominio
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;



class routeDefaultDispatcher extends Controller
{
    //

    function routerDefault(Request $request)
    {
        // dd($request->query());

        $domain = $request->getHttpHost();
        $qs = $request->query();
        switch ($domain) {

            case 'landings.e-retaildata.com':
            case 'tuofertadeseguro.com':
            case 'www.tuofertadeseguro.com':
            case 'plan-dental.tuofertadeseguro.com':
                //Local Development
                //   dd('aquí hemos llegado');
                return redirect()->route('metlife-plan-dental-plus_1', $qs);
                break;
            case 'mkmegusta.er.mmtseguros.es':
                return redirect()->route('mmt-seguros-landing_1', $qs);

                break;
            case 'keycoes.e-retaildata.com':
            case 'mmt-landings.test':
            case 'informate.keycoes.es':
                return redirect()->route('keycoes-landing_1', $qs);
                break;
            default:
                return redirect()->route('metlife-plan-dental-plus_1', $qs);

                break;
        }
        // dd($request->getHttpHost());
    }
}
