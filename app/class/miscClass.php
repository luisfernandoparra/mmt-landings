<?php
/*
 * Created on Mon May 20 2019
 *
 * Copyright (c) 2019 e-retail data Technology SL
 * 
 * Clase que contendrá funciones comunes y misc.
 * 
 * 
 * 
 */

namespace App\miscClass;

use Carbon\Carbon;
use GuzzleHttp\Client;
use DB;

class miscClass
{
    /**
     * Máximos registros a procesar por ejecución del WS
     *
     * @var integer
     */
    protected $ws_max_registers = 10;
    /**
     * Identificador del cliente. Necesario para la query
     *
     * @var integer
     */
    private $client_id;
    /**
     * Función que recibe un email, y chequea si existe utilizando la api existente 
     *
     * @param string $email Email to check
     * @param string $clientName Default mmt. Name of the client. Needed for getting Api information such as User and password
     * @return string json format
     */
    public function emailCheck($email, $clientName = 'mmt')
    {
        $email = strtolower(trim($email));
          
        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            //dd($email);
            return 0;
        } else {
            $usr = config('misc.emailChecker.clients.' . $clientName . '.usr');
            $pwd = config('misc.emailChecker.clients.' . $clientName . '.pwd');
            $url = config('misc.emailChecker.url');
            $url = sprintf($url, $usr, $pwd, $email);
            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', $url);
            
            $statusCode = $response->getStatusCode(); # 200
            $statusContent = $response->getHeaderLine('content-type'); # 'application/json; charset=utf8'
            $resp = $response->getBody();
            //dump($resp);
            //dump($statusCode);
            $respJson = json_decode($resp, true);
            $exists = $respJson['success'];
            //dump($exists);
            //dump((int)$exists);
            $statusId = $respJson['statusId'];
            //dd($respJson);

            return (int)$exists;
            /* $response = ['validEmail' => (int)$exists];
            if (!$exists) {
                $response['errors']=['El email no es correcto o no existe'];
            }
            session(['emailValid' => $exists]);
            */
        }

        // return response()->json($response);
    }

/**
 * Función tonta. Recibe una fecha formato YYYY-MM-DD y la devuelve DD-MM-YYYY
 * 
 *
 * @param string $date
 * @return mixed False si hay un error en la fecha. e.o.c la fecha modificada
 */
    public function dateReverse($date) {
        $tmp = explode('-',$date);
        if (count($tmp)!=3) return false;
        $yy = $tmp[0];
        if (strlen($yy)!=4) return false;
        $mm = str_pad($tmp[1],2,'0',STR_PAD_LEFT);
        $dd = str_pad($tmp[2],2,'0',STR_PAD_LEFT);
        if (strlen($mm)!=2) return false;
        if (strlen($dd)!=2) return false;
        return $dd."-".$mm."-".$yy;
    }


    /**
     * *************************************************************************
     *          FUNCIONES CRON-WS-COMMANDS
     * *************************************************************************
     */

    /**
     * Función pública que devuelve una colección de los datos a procesar
     *
     * @param integer $client_id Identificador del cliente. 
     * @param integer $ws_status DEFAULT 0. Estado de los datos a devolver. 
     * @return void
     */
    public function ws_getRegisters($client_id, $ws_status = 0)
    {
        $this->client_id = $client_id;
        $data = DB::table('registers')->join('sites', function ($join) {
            $join->on('registers.id_site', '=', 'sites.id')
                ->where('sites.id_client', $this->client_id);
        })->where('b_sent_client', $ws_status)->select(['registers.id', 'name','surname','surname2','gender', 'email', 'phone','zip', 'ip', 'add_info_1', 'add_info_2', 'b_accept_cookies', 'b_accept_legal_issues', 'registers.created_at'])->limit($this->ws_max_registers)->get();
        return $data;
    }

    /**
     * Función pública que pone en semáforo el dato a procesar para que nadie más lo intente hacer
     *
     * @param integer $id Identificador de la tabla a poner en cuarentena
     * @param integer $status Estado que vamos a poner al registro
     * @param boolean $updateDate Si es true actualizamos también el campo de date_sent_client
     * @return void
     */
    public function ws_setRegisterStatus($id,$status,$updateDate=false)
    {
        $arrData = ['b_sent_client' => $status,'updated_at'=>Carbon::now()->format('Y-m-d H:i:s')];
        if ($updateDate) {
            $arrData['date_sent_client'] = Carbon::now()->format('Y-m-d H:i:s');
        }
        DB::table('registers')
            ->where('id', $id)
            ->update($arrData);
    }

    /**
     * Función que inicia el envío de datos al cliente
     *
     * @param integer $id identificador de la tabla registers
     * @param string $protocol protocolo de envío
     * @param string $url Url a donde mandamos los datos. Podría ser igual que el campo $data
     * @param string $data DAtos a mandar. POdría contener la url
     * @param mixed $header Objeto o array o string. Puede venir vacío. 
     * @param string $additionalInfo Información adicional
     * @param boolean $setSemaphore Si es true, le ponemos en semáforo
     * @return integer Identificador del id de la tabla local
     */
    public function ws_setStartSent($id,$protocol,$url,$data, $header,$additionalInfo,$setSemaphore) {
        if ($setSemaphore) {
            $this->ws_setRegisterStatus($id,2,true);
        }
        if (is_object($data)|| is_array($data)) {
            $data = json_encode($data);
        }
        if (!empty($header) && (is_object($header)||is_array($header))) {
            $header = json_encode($header);
        }
        $id = DB::table('registers_wsdata')->insertGetId([
            'registers_id'=>$id,
            'b_status'=>2,
            'sent_protocol'=>$protocol,
            'sent_url'=>$url,
            'sent_data'=>$data,
            'sent_header'=>$header,
            'sent_at'=>Carbon::now()->format('Y-m-d H:i:s')
        ]);
        return $id;
    }

    /**
     * Función que se ejecuta una vez que tenemos todos los datos de respuesta a la hora de enviar los datos
     *
     * @param integer $id Identificador local de la tabla registers_wsdata
     * @param integer $register_id identificador de la tabla registers
     * @param boolean $b_status Estado del envio 
     * @param integer $resp_http_status estado de la respuesta
     * @param mixed $resp_raw respuesta tal cuál la hemos recibido del cliente
     * @param mixed $resp_header respuesta tal cuál la hemos recibido del cliente
     * @return void
     */
    public function ws_setEndSent($id,$register_id,$b_status,$resp_http_status,$resp_raw,$resp_header) {
        $this->ws_setRegisterStatus($register_id,1,true);
        if (is_object($resp_raw)|| is_array($resp_raw)) {
            $resp_raw = json_encode($resp_raw);
        }
        if (is_object($resp_header)|| is_array($resp_header)) {
            $resp_header = json_encode($resp_header);
        }
        DB::table('registers_wsdata')
            ->where('id', $id)
            ->update([
                'b_status'=>$b_status,
                'resp_http_status'=>$resp_http_status,
                'resp_raw'=>$resp_raw,
                'resp_header'=>$resp_header,
                'resp_at'=>Carbon::now()->format('Y-m-d H:i:s')
                ]);
    }

    /**
     * *************************************************************************
     *          END FUNCIONES CRON-WS-COMMANDS
     * *************************************************************************
     */
}
