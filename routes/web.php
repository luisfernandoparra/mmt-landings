<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});
*/
/*Route::get('/',function () {
    return redirect('/tu-seguro-hogar');
    //redirect()->route('mmt-seguros-landing_1')
});
*/

Route::get('/','routeDefaultDispatcher@routerDefault');

Route::get('/form-test', 'Forms\formTestController@index');
Route::post('/form-test','Forms\formTestController@insertRegister');
Route::post('/check-email-test','Forms\formTestController@checkEmail')->name('check-email-test');

/**
 * Rutas comunes a todos los sites
 */
Route::post('/check-email','common\miscController@checkEmail')->name('check-email');


/**
 * Rutas MMT Seguros. Campaña 1: ¿Tu seguro de hogar te devuelve dinero si no lo usas?
 */
Route::get('/tu-seguro-hogar','mmt\siteController@indexCampaign_1')->name('mmt-seguros-landing_1');
Route::post('/tu-seguro-hogar','mmt\siteController@insertRegisterCampaign_1');

// Route::get('/mmt-seguros/tu-seguro-hogar', function() {    return view('mmt.campaign_1.home');});

/**
 * Rutas MetLife PLan Dental
 */
/**
 * 2010.01.20
 */
Route::get('plan-dental-plus','metlife\siteController@indexCampaign_1')->name('metlife-plan-dental-plus_1');
Route::post('plan-dental-plus','metlife\siteController@insertRegisterCampaign_1');

Route::get('thanks','metlife\siteController@testThanks');


/**
 * @date 2020.07.15
 * Añadimos el cliente KeyCoes (clientid 4)
 */
Route::get('dejanos-tus-datos','keycoes\siteController@indexCampaign_1')->name('keycoes-landing_1');
Route::post('dejanos-tus-datos','keycoes\siteController@insertRegisterCampaign_1');

Route::get('gracias-por-contactar-con-keycoes','keycoes\siteController@gracias_1')->name('keycoes-thanks');

