<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
//use DB;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->bigIncrements('id')->autoIncrement();
            $table->string('client');
            $table->timestamp('created_at')->useCurrent();
$table->timestamp('updated_at')->useCurrent();
           // $table->timestamps();
        });
        DB::table('clients')->insert([['client'=>'mmt']]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
