<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistersWsdataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registers_wsdata', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('registers_id')->unsigned()->comment('FK a la tabla registers');
            $table->boolean('b_status')->default(0)->comment('0: error, 1: correcto');
            $table->string('sent_protocol')->comment('forma de envío, GET, POST, XML, POST JSON... cualquiera');
            $table->longText('sent_url')->comment('Url a donde enviamos los datos. Pudiera ser de cualquier clase o info e igual a sent_Data');

            $table->longText('sent_data')->comment('Datos enviados. Pudiera ser de cualquier clase o info');
            $table->longText('sent_header')->nullable()->comment('Cabecera enviada. Si es vacía no hemos enviado una cabecera manipulada');

            $table->integer('resp_http_status')->unsigned()->default(0)->comment('Respuesta HTTP del envío');
            $table->longText('resp_raw')->nullable()->comment('Respuesta del cliente');
            $table->longText('resp_header')->nullable()->comment('Header de la respuesta del cliente');

            $table->timestamp('sent_at')->useCurrent();
            $table->timestamp('resp_at')->nullable();
            $table->foreign('registers_id')->references('id')->on('registers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registers_wsdata');
    }
}
