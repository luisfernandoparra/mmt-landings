<?php


use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;

class CreateLegalEpigraphsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('legal_epigraphs_types', function (Blueprint $table) {
            $table->integer('id')->unsigned()->autoIncrement();
            $table->string('legal_epigraphs_type');
            $table->timestamp('created_at')->useCurrent();
$table->timestamp('updated_at')->useCurrent();
            //$table->timestamps();
        });
        DB::table('legal_epigraphs_types')->insert([
            ['legal_epigraphs_type'=>'Rgpd Info','created_at' => Carbon::now()->format('Y-m-d H:i:s')],
            ['legal_epigraphs_type'=>'Cookie Info','created_at' => Carbon::now()->format('Y-m-d H:i:s')]
        ]);
        Schema::create('legal_epigraphs', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned()->autoIncrement();
            $table->integer('id_legal_epigraphs_types')->unsigned();
            $table->string('title');
            $table->longText('epigraph');
            $table->timestamp('created_at')->useCurrent();
$table->timestamp('updated_at')->useCurrent();
            //$table->timestamps();
            $table->foreign('id_legal_epigraphs_types')->references('id')->on('legal_epigraphs_types');
            $table->comment = 'Distintos epígrafes existentes';
        });
        Schema::create('legal_epigraphs_sites', function (Blueprint $table) {
            $table->bigInteger('id_site')->unsigned();
            $table->bigInteger('id_legal_epigraph')->unsigned();
            $table->tinyInteger('order')->unsigned()->default(0);
            $table->boolean('b_active')->default(true);
            $table->timestamp('created_at')->useCurrent();
$table->timestamp('updated_at')->useCurrent();
            //$table->timestamps();
            $table->primary(array('id_site','id_legal_epigraph'));
            $table->foreign('id_site')->references('id')->on('sites');
            $table->foreign('id_legal_epigraph')->references('id')->on('legal_epigraphs');
            $table->comment= 'Tabla que contiene las distintas "porciones" de epígrafes para un site';
        });
        
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::dropIfExists('legal_epigraphs_sites');
        
        Schema::dropIfExists('legal_epigraphs');
        
        Schema::dropIfExists('legal_epigraphs_types');
        
    }
}
