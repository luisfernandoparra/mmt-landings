<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sites', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('id_client')->unsigned();
            $table->string('site_name');
            $table->timestamp('created_at')->useCurrent();
$table->timestamp('updated_at')->useCurrent();
           // $table->timestamps();
            $table->foreign('id_client')->references('id')->on('clients');
            $table->comment = 'Distintos Sites existentes';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sites');
    }
}
