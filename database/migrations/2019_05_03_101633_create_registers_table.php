<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_site')->unsigned()->comment('Identificador de donde se ha registrado el usuario');
            $table->bigInteger('id_source')->unsigned()->comment('FK a la tabla de sources');
            $table->integer('id_province')->unsigned()->default(0)->comment('Posible Provincia');
            $table->string('gender',5)->nullable()->comment('Género del usuario');
            $table->string('name')->nullable()->comment('Nombre del registro');
            $table->string('surname')->nullable();
            $table->string('surname2')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('zip',5)->nullable()->comment('CP introducido');
            $table->string('ip',15)->nullable()->comment('Ip del usuario');
            $table->longText('user_agent')->nullable()->comment('User Agent del usuario');
            $table->string('add_info_1')->nullable()->comment('Campo de uso libre');
            $table->string('add_info_2')->nullable()->comment('Campo de uso libre');
            $table->boolean('b_accept_cookies')->default(0)->comment('El usuario aceptó cookies');
            $table->timestamp('date_accept_cookies')->nullable()->comment('Fecha aceptación Cookies');
            $table->boolean('b_accept_legal_issues')->default(0)->comment('Usuario acepta cuestiones legales');
            $table->timestamp('date_accept_legal_issues')->nullable()->comment('Fecha aceptación cuestiones legales');
            $table->boolean('b_sent_client')->default(0)->comment('Si el dato ha sido extraído o enviado al cliente');
            $table->timestamp('date_sent_client')->nullable()->comment('Fecha de extraccón o envío al cliente');
            $table->timestamp('created_at')->useCurrent();
$table->timestamp('updated_at')->useCurrent();
            $table->foreign('id_site')->references('id')->on('sites');
            $table->foreign('id_province')->references('id')->on('provinces');
            $table->foreign('id_source')->references('id')->on('sources');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registers');
    }
}
