<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProvincesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('provinces', function (Blueprint $table) {
            $table->integer('id')->unsigned()->primary();
            $table->string('province');
        });
        DB::table('provinces')->insert([
            ['id' => 0, 'province' => 'Unknown'],

            ['id' => 1, 'province' => 'Álava'],
            ['id' => 2, 'province' => 'Albacete'],
            ['id' => 3, 'province' => 'Alicante'],
            ['id' => 4, 'province' => 'Almería'],
            ['id' => 5, 'province' => 'Ávila'],
            ['id' => 6, 'province' => 'Badajoz'],
            ['id' => 7, 'province' => 'Balears (Illes)'],
            ['id' => 8, 'province' => 'Barcelona'],
            ['id' => 9, 'province' => 'Burgos'],
            ['id' => 10, 'province' => 'Cáceres'],
            ['id' => 11, 'province' => 'Cádiz'],
            ['id' => 12, 'province' => 'Castellón de la Plana'],
            ['id' => 13, 'province' => 'Ciudad Real'],
            ['id' => 14, 'province' => 'Córdoba'],
            ['id' => 15, 'province' => 'Coruña (La)'],
            ['id' => 16, 'province' => 'Cuenca'],
            ['id' => 17, 'province' => 'Girona'],
            ['id' => 18, 'province' => 'Granada'],
            ['id' => 19, 'province' => 'Guadalajara'],
            ['id' => 20, 'province' => 'Guipúzcoa'],
            ['id' => 21, 'province' => 'Huelva'],
            ['id' => 22, 'province' => 'Huesca'],
            ['id' => 23, 'province' => 'Jaén'],
            ['id' => 24, 'province' => 'León'],
            ['id' => 25, 'province' => 'Lleida'],
            ['id' => 26, 'province' => 'Rioja (La)'],
            ['id' => 27, 'province' => 'Lugo'],
            ['id' => 28, 'province' => 'Madrid'],
            ['id' => 29, 'province' => 'Málaga'],
            ['id' => 30, 'province' => 'Murcia'],
            ['id' => 31, 'province' => 'Navarra'],
            ['id' => 32, 'province' => 'Orense'],
            ['id' => 33, 'province' => 'Asturias'],
            ['id' => 34, 'province' => 'Palencia'],
            ['id' => 35, 'province' => 'Palmas (Las)'],
            ['id' => 36, 'province' => 'Pontevedra'],
            ['id' => 37, 'province' => 'Salamanca'],
            ['id' => 38, 'province' => 'Santa Cruz de Tenerife'],
            ['id' => 39, 'province' => 'Cantabria'],
            ['id' => 40, 'province' => 'Segovia'],
            ['id' => 41, 'province' => 'Sevilla'],
            ['id' => 42, 'province' => 'Soria'],
            ['id' => 43, 'province' => 'Tarragona'],
            ['id' => 44, 'province' => 'Teruel'],
            ['id' => 45, 'province' => 'Toledo'],
            ['id' => 46, 'province' => 'Valencia'],
            ['id' => 47, 'province' => 'Valladolid'],
            ['id' => 48, 'province' => 'Vizcaya'],
            ['id' => 49, 'province' => 'Zamora'],
            ['id' => 50, 'province' => 'Zaragoza'],
            ['id' => 51, 'province' => 'Ceuta'],
            ['id' => 52, 'province' => 'Melilla']
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('provinces');
    }
}
