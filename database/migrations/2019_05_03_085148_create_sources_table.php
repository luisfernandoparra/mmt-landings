<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sources', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('source');
            $table->timestamp('created_at')->useCurrent();
$table->timestamp('updated_at')->useCurrent();
           // $table->timestamps();
        });
        DB::table('sources')->insert([
            ['id'=>1450,'source'=>'Source Lfp','created_at' => Carbon::now()->format('Y-m-d H:i:s')],
            ['id'=>124,'source'=>'El Otro','created_at' => Carbon::now()->format('Y-m-d H:i:s')],
            ['id'=>0,'source'=>'Unknown','created_at' => Carbon::now()->format('Y-m-d H:i:s')]

            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sources');
    }
}
