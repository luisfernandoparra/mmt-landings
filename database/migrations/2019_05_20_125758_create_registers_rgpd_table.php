<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistersRgpdTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registers_rgpd', function (Blueprint $table) {
            $table->bigInteger('registers_id')->unsigned()->comment('FK a la tabla registers');
            $table->bigInteger('legal_epigraphs_id')->unsigned()->comment('FK a la tabla legal_epigraphs');
            $table->longText('epigraph')->comment('Aún pudiendo ser redundante, cogemos exactamente lo que se mostraba en la web en el momento del registro');
            $table->timestamp('created_at')->useCurrent();
            $table->foreign('registers_id')->references('id')->on('registers');
            $table->foreign('legal_epigraphs_id')->references('id')->on('legal_epigraphs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registers_rgpd');
    }
}
