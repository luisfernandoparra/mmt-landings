<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

# Summary

## About MMT-Landings

Servidor de Landings, en principio para el cliente MMT desarrollado por e-retail data technology.
Versión inicial: 2019.05.20
El sistema además posee un cron que lanza los datos por cliente vía Api

Es una actualización del existente pero desarrollado en:

- PHP 7
- Laravel Framework
- Nginx
- MySql 

Para más detalles de las librerías utilizadas, por favor, chequee el fichero composer.json

## First Deploy
1. Setup deploy ssh keys at gitlab project configuration.
2. [Install composer](https://getcomposer.org/download/) and [Setup globally](https://getcomposer.org/doc/00-intro.md#globally)
3. `git clone git@gitlab.com:e-retail-data/mmt-landings.git mmt-landings`
4. `cd mmt-landings`
5. `cp .env.example .env` and setup .env with correct values.
6. `composer install --no-dev`
7. `php artisan migrate` will populate database
8.  `php artisan config:cache`
9.  `php artisan route:cache`


## Update & deploy
```bash
cd /path-to-your-project/
```
As we have the project deployed as nginx we need to switch to nginx user (this user also has the deploy key configured at git server)

```bash
sudo -Hu nginx bash
```
Now we are at project root path and we are the right user

```bash
git pull origin master && composer install --no-dev && php artisan migrate &&  php artisan config:cache && php artisan route:cache
```

Don't forget to execute the following code for updates:
```bash
php artisan config:clear && php artisan cache:clear && php artisan config:cache
```

# License

Copyright by e-retail data Technology 2019 [e-retail](http://www.e-retailadvertising.com/).

